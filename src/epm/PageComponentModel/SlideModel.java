package epm.PageComponentModel;

import epm.PageComponentEditNodes.SlideEditor;

/**
 * This class represents a single slide in a slide show.
 * 
 * @author McKilla Gorilla & Brian Kondracki
 */
public class SlideModel {
    String imagePath;
    String imageFileName;
    String caption;
    SlideShowComponentModel thisSlideShow;
    SlideEditor editor;
     
    /**
     * Constructor, it initializes all slide data.
     * @param initImageFileName File name of the image.
     * 
     * @param initImagePath File path for the image.
     * 
     */
    public SlideModel(String initImagePath, String initCaption, SlideShowComponentModel thisSlideShow) {
	imagePath = initImagePath;
        int lastIndex = initImagePath.lastIndexOf("\\");
        if(lastIndex == -1)
            lastIndex = initImagePath.lastIndexOf("/");
        imageFileName = initImagePath.substring(lastIndex);
        this.caption = initCaption;
        this.thisSlideShow= thisSlideShow;
        
        editor  = thisSlideShow.getEditNode().getSlidesEditor().addSlideEditor(this);
    }
    
    // ACCESSOR METHODS
    public String getImagePath() { return imagePath; }
    public String getImageFileName(){ return imageFileName; }
    public String getCaption(){ return caption; }
    public SlideShowComponentModel getThisSlideShow(){ return thisSlideShow; }
    public SlideEditor getEditor(){ return editor; }
    
    // MUTATOR METHODS
    public void setImagePath(String initImagePath) {
	imagePath = initImagePath;
        editor.setImage();
    }
    
    public void setImageFileName(String initImageFileName){ this.imageFileName = initImageFileName; }
    
    public void setCaption(String caption){
        this.caption = caption;
    }
    
    public void setThisSlideShow(SlideShowComponentModel newSlideShow){
        thisSlideShow = newSlideShow;
    }
}
