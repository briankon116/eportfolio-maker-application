/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epm.PageComponentModel;

import epm.PageComponentEditNodes.TextComponentEditNode;
import java.util.ArrayList;

/**
 *
 * @author briankondracki
 */
public class TextComponentModel extends PageComponentModel{
    private String text;
    private int font;
    private ArrayList<HyperlinkComponentModel> hyperlinks;
    
    public TextComponentModel(String title, String text){
        super("Textfield", title);
        font = 0;
        this.text = text;
        setEditNode(new TextComponentEditNode(this));
        hyperlinks = new ArrayList();
    }
    
    public void addHyperlink(String url, String text, int startIndex, int endIndex){
        hyperlinks.add(new HyperlinkComponentModel(startIndex, endIndex, url, text, this));
    }
    
    public String getText(){ return text; }
    public int getFont(){ return font; }
    public ArrayList<HyperlinkComponentModel> getHyperlinks(){ return hyperlinks; }
    public TextComponentEditNode getEditNode(){ return (TextComponentEditNode)super.getEditNode(); }
    
    public void setText(String text){ this.text = text; }
    public void setFont(int font){ this.font = font; }
}
