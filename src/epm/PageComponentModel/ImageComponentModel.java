/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epm.PageComponentModel;

import epm.PageComponentEditNodes.ImageComponentEditNode;

/**
 *
 * @author briankondracki
 */
public class ImageComponentModel extends PageComponentModel{
    private String path;
    private String fileName;
    private int floatImage;
    
    public ImageComponentModel(String title){
        super("Image",title);
        path = " ";
        fileName = " ";
        floatImage = 0;
        setEditNode(new ImageComponentEditNode(this));
    }
    
    public ImageComponentModel(String title, String path, String fileName){
        super("Image",title);
        setEditNode(new ImageComponentEditNode(this));
        this.path = path;
        this.fileName = fileName;
        floatImage = 0;
    }
    
    public String getPath(){ return path; }
    public String getFileName(){ return fileName; }
    public ImageComponentEditNode getEditNode(){ return (ImageComponentEditNode)super.getEditNode(); }
    public int getFloatImage(){ return floatImage; }
    
    public void setPath(String path){ this.path = path; getEditNode().setNewThumbnail(); }
    public void setFileName(String fileName){ this.fileName = fileName; }
    public void setFloatImage(int index){ this.floatImage = index; }
}
