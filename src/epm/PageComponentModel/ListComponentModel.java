/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epm.PageComponentModel;

import epm.PageComponentEditNodes.ListComponentEditNode;
import java.util.ArrayList;

/**
 *
 * @author briankondracki
 */
public class ListComponentModel extends PageComponentModel{
    private int font;
    private ArrayList<String> listItems;
    
    public ListComponentModel(String title){
        super("List", title);
        font = 0;
        setEditNode(new ListComponentEditNode(this));
        listItems = new ArrayList();
    }
    
    public ListComponentModel(String title, ArrayList<String> initListItems){
        super("List", title);
        setEditNode(new ListComponentEditNode(this));
        this.listItems = initListItems;
    }
    
    public int getFont(){ return font; }
    public ArrayList getListItems(){ return listItems; }
    public ListComponentEditNode getEditNode(){ return (ListComponentEditNode)super.getEditNode(); }
    
    public void addListItem(String initListText){
        listItems.add(initListText);
        getEditNode().getListItems().addListItem(initListText);
    }
    
    public void removeListItem(int index){
        listItems.remove(index);
        getEditNode().getListItems().removeListItem(index);
    }
    
    public void setFont(int index){ this.font = index; }
}
