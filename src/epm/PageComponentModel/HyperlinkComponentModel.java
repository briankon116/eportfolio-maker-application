/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epm.PageComponentModel;

import epm.PageComponentEditNodes.HyperlinkComponentEditNode;

/**
 *
 * @author briankondracki
 */
public class HyperlinkComponentModel extends PageComponentModel{
    private int startIndex;
    private int endIndex;
    private String url;
    private String text;
    private HyperlinkComponentEditNode editNode;
    private TextComponentModel textModel;
    
    public HyperlinkComponentModel(int initStartIndex, int initEndIndex, String initURL, String initText, TextComponentModel textModel){
        super("Hyperlink");
        
        startIndex = initStartIndex;
        endIndex = initEndIndex;
        url = initURL;
        text = initText;
        
        editNode = new HyperlinkComponentEditNode(url, this);
        this.textModel = textModel;
    }
    
    public int getStartIndex(){ return startIndex; }
    public int getEndIndex(){ return endIndex; }
    public String getURL(){ return url; }
    public String getText(){ return text; }
    public HyperlinkComponentEditNode getHyperlinkEditNode(){ return editNode; }
    public TextComponentModel getTextModel(){ return textModel; }
    
    public void setURL(String URL){ this.url = URL; }
}
