/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epm.PageComponentModel;

import epm.PageComponentEditNodes.VideoComponentEditNode;

/**
 *
 * @author briankondracki
 */
public class VideoComponentModel extends PageComponentModel{
    private String path;
    private String fileName;
    private int size;
    
    public VideoComponentModel(String title){
        super("Video", title);
        path = " ";
        fileName = " ";
        setEditNode(new VideoComponentEditNode(this));
    }
    
    public VideoComponentModel(String title, String path){
        super("Video", title);
        setEditNode(new VideoComponentEditNode(this));
        this.path = path;
    }
    
    public String getPath(){ return path; }
    public String getFileName(){ return fileName; }
    public int getSize(){ return size; }
    public VideoComponentEditNode getEditNode(){ return (VideoComponentEditNode)super.getEditNode(); }
    
    public void setPath(String path){
        this.path = path; 
        this.fileName=path.substring(path.lastIndexOf('\\')+1,path.length());
    }
    
    public void setSize(int size){
        this.size = size;
    }
}
