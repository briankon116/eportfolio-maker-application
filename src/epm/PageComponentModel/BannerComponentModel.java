/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epm.PageComponentModel;

import epm.PageComponentEditNodes.BannerComponentEditNode;

/**
 *
 * @author briankondracki
 */
public class BannerComponentModel extends PageComponentModel{
    private String bannerText;
    private String footerText;
    private String bannerImageFileName;
    private String bannerImagePath;
    private BannerComponentEditNode editNode;
 
    public BannerComponentModel(String bannerText, String footerText, String bannerImageFileName, String bannerImagePath){
        super("Banner");
        this.bannerText=bannerText;
        this.footerText = footerText;
        this.bannerImageFileName = bannerImageFileName;
        this.bannerImagePath = bannerImagePath;
        editNode = new BannerComponentEditNode(this);
    }
    
    public String getBannerText(){ return bannerText; }
    public String getFooterText(){ return footerText; }
    public String getBannerImageFileName(){ return bannerImageFileName; }
    public String getBannerImagePath(){ return bannerImagePath; }
    public BannerComponentEditNode getEditNode(){ return editNode; }
    
    public void setBannerText(String text){ 
        this.bannerText = text;
    }
    public void setFooterText(String text){
        this.footerText = text;
    }
    public void setBannerImageFileName(String fileName){ 
        bannerImageFileName = fileName; 
    }
    public void setBannerImagePath(String path){ 
        bannerImagePath = path; 
        setBannerImageFileName(path.substring(path.lastIndexOf('\\')+1, path.length()));
        editNode.setNewThumbnail();
    }
}
