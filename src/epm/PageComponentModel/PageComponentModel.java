/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epm.PageComponentModel;

import epm.PageComponentEditNodes.PageComponentEditNode;


/**
 *
 * @author briankondracki
 */
public class PageComponentModel {
    private String type;
    private String title;
    private PageComponentEditNode editNode;

    
    public PageComponentModel(String type){
        this.type = type; 
        title = "";
    }
    
    public PageComponentModel(String type, String title){
        this.type = type;
        this.title = title;
    }
    
    public PageComponentModel(String type, String title, PageComponentEditNode editNode){
        this.type = type;
        this.title = title;
        this.editNode = editNode;
    }
    
    public String getType(){ return type; }
    public String getTitle(){ return title; }
    public PageComponentEditNode getEditNode(){ return editNode; }
    
    public void setType(String type){ this.type = type; }
    public void setTitle(String title){ this.title = title; }
    public void setEditNode(PageComponentEditNode editNode){ this.editNode = editNode; }
}
