/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epm.PageComponentModel;

import epm.PageComponentEditNodes.SlideShowComponentEditNode;
import java.util.Collections;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author briankondracki
 */
public class SlideShowComponentModel extends PageComponentModel{
    private String title;
    private ObservableList<SlideModel> slides;
    
    public SlideShowComponentModel(String title) {
	super("Slide Show", title);
        setEditNode(new SlideShowComponentEditNode(this));
	slides = FXCollections.observableArrayList();
	//reset();	
    }
    
    public ObservableList<SlideModel> getSlides() {
	return slides;
    }
    
    @Override
    public SlideShowComponentEditNode getEditNode(){ return (SlideShowComponentEditNode)super.getEditNode(); }
    
    /**
     * Adds a slide to the slide show with the parameter settings.
     * @param initImageFileName File name of the slide image to add.
     * @param initImagePath File path for the slide image to add.
     */
    public void addSlide(String imagePath, String initCaption) {
	SlideModel slideToAdd = new SlideModel(imagePath, initCaption, this);
	slides.add(slideToAdd);
	//ui.reloadSlideShowPane(this);
    }
    
    public void addSlide(SlideModel slide){
        slides.add(slide);
    }
    
    public void removeSlide(int index){
        slides.remove(index);
        getEditNode().getSlidesEditor().removeSlideEditor(index);
        //ui.reloadSlideShowPane(this);
    }

    public void moveSlideLeft(int index){
        if(index==0)
            return;
        else{
            Collections.swap(slides, index, index-1);
            getEditNode().getSlidesEditor().moveSlideLeft(index);
        }
    }
    
    public void moveSlideRight(int index){
        if(index==slides.size()-1)
            return;
        else{
            Collections.swap(slides,index,index+1);
            getEditNode().getSlidesEditor().moveSlideRight(index);
        }
    }
    
    /**
     * Resets the slide show to have no slides and a default title.
     */
    public void reset() {
	slides.clear();
	title = "Enter Slide Show Title";
    }
}
