/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epm.controller;

import SiteModel.PageModel;
import SiteModel.SiteModel;
import epm.PageComponentModel.BannerComponentModel;
import epm.PageComponentModel.HyperlinkComponentModel;
import epm.PageComponentModel.ImageComponentModel;
import epm.PageComponentModel.ListComponentModel;
import epm.PageComponentModel.PageComponentModel;
import epm.PageComponentModel.SlideModel;
import epm.PageComponentModel.SlideShowComponentModel;
import epm.PageComponentModel.TextComponentModel;
import epm.PageComponentModel.VideoComponentModel;
import epm.view.SiteEditView;
import java.io.File;
import java.io.FileInputStream;

import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.ObservableList;
import javafx.scene.control.Tab;
import javafx.scene.web.WebView;
import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonArrayBuilder;
import javax.json.JsonObject;
import javax.json.JsonReader;
import javax.json.JsonWriter;
import org.apache.commons.io.FileUtils;

/**
 *
 * @author Brian
 */
public class FileController{
    //JSON FILE READING AND WRITING CONSTANTS
    public static String JSON_TITLE = "title";
    public static String JSON_TYPE = "type";
    public static String JSON_TEMPLATE = "template";
    public static String JSON_FONT = "font";
    public static String JSON_IMAGE_FILE_NAME = "image_file_name";
    public static String JSON_IMAGE_PATH = "image_path";
    public static String JSON_EXT = ".json";
    public static String SLASH = "/";
    public static String SITE_SAVES_PATH = "./saves/";
    
    private SiteModel site;
    
    private EventController controller;
    
    public FileController(SiteModel initSite){
        site = initSite;
        controller = EventController.getInstance();
    }
    
    public void setSite(SiteModel initSite){
        site = initSite;
    }
    
    public void saveSite()throws IOException{
        //BUILD THE FILE PATH
        String siteTitle = "" + site.getSiteTitle();
        String jsonFilePath = SITE_SAVES_PATH + siteTitle + JSON_EXT;
        
        //INIT THE WRITER
        OutputStream os = new FileOutputStream(jsonFilePath);
        JsonWriter jsonWriter = Json.createWriter(os);
        
        //BUILD THE ARRAY OF PAGES IN THE SITE
        JsonArray pagesJsonArray = makePagesJsonArray(site.getPages());
        
        //BUILD THE SITE JSON FILE
        JsonObject siteJsonObject = Json.createObjectBuilder()
                .add("title", siteTitle)
                .add("pages", pagesJsonArray)
                .build();
        jsonWriter.writeObject(siteJsonObject);
        jsonWriter.close();
    }
    
    private JsonArray makePagesJsonArray(ArrayList<PageModel> pages){
        JsonArrayBuilder jsb = Json.createArrayBuilder();
        for(PageModel page: pages){
            JsonObject jso = makePageJsonObject(page);
            jsb.add(jso);
        }
        JsonArray jA = jsb.build();
        return jA;
    }
    
    private JsonObject makePageJsonObject(PageModel page){
        JsonObject jso = Json.createObjectBuilder()
                .add(JSON_TITLE, page.getTitle())
                .add("fileName", page.getFileName())
                .add(JSON_TEMPLATE, page.getTemplate())
                .add("colorScheme", page.getColorScheme())
                .add(JSON_FONT, page.getFont())
                .add("banner", makeBannerJsonObject(page.getBanner()))
                .add("components", makeComponentsJsonArray(page.getComponents()))
                .build();
        return jso;
    }
    
    private JsonObject makeBannerJsonObject(BannerComponentModel banner){
        JsonObject jso = Json.createObjectBuilder()
                .add("type", banner.getType())
                .add("bannerText", banner.getBannerText())
                .add("bannerImageFileName", banner.getBannerImageFileName())
                .add("bannerImagePath", banner.getBannerImagePath())
                .build();
                return jso;
    }
    
    private JsonArray makeComponentsJsonArray(ArrayList<PageComponentModel> components){
        JsonArrayBuilder jsb = Json.createArrayBuilder();
        JsonObject jso;
        for(PageComponentModel component: components){
            if(component.getType()=="Image"){
                ImageComponentModel imageComponent = (ImageComponentModel)component;
                jso = makeImageComponentJsonObject(imageComponent);
            }
            else if(component.getType()=="List"){
                ListComponentModel listComponent = (ListComponentModel)component;
                jso = makeListComponentJsonObject(listComponent);
            }
            else if(component.getType() == "Video"){
                VideoComponentModel videoComponent = (VideoComponentModel)component;
                jso = makeVideoComponentJsonObject(videoComponent);
            }
            else if(component.getType() == "Slide Show"){
                SlideShowComponentModel slideShowComponent = (SlideShowComponentModel)component;
                jso = makeSlideShowComponentJsonObject(slideShowComponent);
            }
            else if(component.getType() == "Textfield"){
                TextComponentModel textComponent = (TextComponentModel)component;
                jso = makeTextComponentJsonObject(textComponent);
            }
            else
                jso = null;
                
            jsb.add(jso);
        }
        JsonArray jA = jsb.build();
        return jA;
    }
    
    private JsonObject makeImageComponentJsonObject(ImageComponentModel component){
            JsonObject jso = Json.createObjectBuilder()
                    .add("type", component.getType())
                    .add("title", component.getTitle())
                    .add("path", component.getPath())
                    .add("fileName", component.getFileName())
                    .add("floatImage", component.getFloatImage())
                    .build();
            return jso;
    }
    
    private JsonObject makeListComponentJsonObject(ListComponentModel component){
        JsonObject jso = Json.createObjectBuilder()
                .add("type", component.getType())
                .add("title", component.getTitle())
                .add("font", component.getFont())
                .add("listItems", makeListItemsJsonArray(component.getListItems()))
                .build();
        return jso;
    }
    
    private JsonArray makeListItemsJsonArray(ArrayList<String>listItems){
        JsonArrayBuilder jsb = Json.createArrayBuilder();
        for(String s: listItems)
            jsb.add(s);
        JsonArray jA = jsb.build();
        
        return jA;
    }
    
    private JsonObject makeVideoComponentJsonObject(VideoComponentModel component){
        JsonObject jso = Json.createObjectBuilder()
                .add("type", component.getType())
                .add("title", component.getTitle())
                .add("path", component.getPath())
                .add("fileName", component.getFileName())
                .add("size", component.getSize())
                .build();
        return jso;
    }
    
    private JsonObject makeSlideShowComponentJsonObject(SlideShowComponentModel component){
        JsonObject jso = Json.createObjectBuilder()
                .add("type", component.getType())
                .add("title", component.getTitle())
                .add("slides", makeSlidesJsonArray(component.getSlides()))
                .build();
        return jso;
    }
    
    private JsonArray makeSlidesJsonArray(ObservableList<SlideModel> slides){
        JsonArrayBuilder jsb = Json.createArrayBuilder();
        for(SlideModel slide: slides){
            jsb.add(makeSlideJsonObject(slide));
        }
        JsonArray jA = jsb.build();
        return jA;
    }
    
    private JsonObject makeSlideJsonObject(SlideModel slide){
        JsonObject jso = Json.createObjectBuilder()
                .add("imagePath", slide.getImagePath())
                .add("imageFileName", slide.getImageFileName())
                .add("caption", slide.getCaption())
                .build();
        return jso;
    }
    
    private JsonObject makeTextComponentJsonObject(TextComponentModel component){
        JsonObject jso = Json.createObjectBuilder()
                .add("type", component.getType())
                .add("title", component.getTitle())
                .add("text", component.getText())
                .add("font", component.getFont())
                .add("hyperlinks",makeHyperlinksJsonArray(component.getHyperlinks()))
                .build();
        return jso;
    }
    
    private JsonArray makeHyperlinksJsonArray(ArrayList<HyperlinkComponentModel>hyperlinks){
        JsonArrayBuilder jsb = Json.createArrayBuilder();
        for(HyperlinkComponentModel hyperlink: hyperlinks){
            jsb.add(makeHyperlinkJsonObject(hyperlink));
        }
        JsonArray jA = jsb.build();
        return jA;
    }
    
    private JsonObject makeHyperlinkJsonObject(HyperlinkComponentModel hyperlink){
        JsonObject jso = Json.createObjectBuilder()
                .add("startIndex", hyperlink.getStartIndex())
                .add("endIndex", hyperlink.getEndIndex())
                .add("text", hyperlink.getText())
                .add("url", hyperlink.getURL())
                .build();
        return jso;
    }
    
    
    
    // LOAD JSON FILE
    public SiteModel loadSite(String path, SiteEditView ui)throws IOException{
        JsonObject json = loadJSONFile(path);
        
        // NOW LOAD THE SITE
        site = new SiteModel();
        site.setSiteTitle(json.getString(JSON_TITLE));
        
        JsonArray jsonPagesArray = json.getJsonArray("pages");
        for(int i = 0; i<jsonPagesArray.size(); i++){
            JsonObject pageObject = jsonPagesArray.getJsonObject(i);
            PageModel newPage = new PageModel(pageObject.getString("title"), pageObject.getString("fileName"));
            newPage.setTemplate(pageObject.getInt("template"));
            newPage.setColorScheme(pageObject.getInt("colorScheme"));
            newPage.setTemplate(pageObject.getInt("font"));
            
            //BANNER
            JsonObject bannerJsonObject = pageObject.getJsonObject("banner");
            newPage.getBanner().getEditNode().getBannerText().getText().setText(bannerJsonObject.getString("bannerText"));
            newPage.getBanner().getEditNode().getSetImageButton().getText().setText(bannerJsonObject.getString("bannerImagePath"));
            newPage.getBanner().setBannerImageFileName(bannerJsonObject.getString("bannerImageFileName"));
            
            JsonArray jsonComponentArray = pageObject.getJsonArray("components");
            
            if(jsonComponentArray!=null){
                for(int j = 0 ; j<jsonComponentArray.size(); j++){
                    JsonObject component = jsonComponentArray.getJsonObject(j);
                    String type = jsonComponentArray.getJsonObject(j).getString("type");

                    if(type.equals("Image")){
                        ImageComponentModel model = new ImageComponentModel(component.getString("title"));
                        model.setTitle(component.getString("title"));
                        model.setPath(component.getString("path"));
                        model.setFileName(component.getString("fileName"));
                        model.setFloatImage(component.getInt("floatImage"));
                        model.getEditNode().getTitleSelector().getText().setText(model.getTitle());
                        model.getEditNode().getSetImageButton().getText().setText(model.getPath());
                        model.getEditNode().getFloatImageSelector().getDropdown().getSelectionModel().select(model.getFloatImage());
                        newPage.addComponent(model);
                    }
                    else if("List".equals(type)){
                        ListComponentModel model = new ListComponentModel(component.getString("title"));
                        model.setFont(component.getInt("font"));
                        model.getEditNode().getFontSelector().getDropdown().getSelectionModel().select(model.getFont());
                        model.getEditNode().getTitleSelector().getText().setText(model.getTitle());
                        for(int k = 0; k < component.getJsonArray("listItems").size(); k++)
                            model.addListItem(component.getJsonArray("listItems").getString(k));

                        newPage.addComponent(model);
                    }
                    else if("Video".equals(type)){
                        VideoComponentModel model = new VideoComponentModel(component.getString("title"));
                        model.setPath(component.getString("path"));
                        model.setSize(component.getInt("size"));
                        model.getEditNode().getTitleSelector().getText().setText(model.getTitle());
                        model.getEditNode().getURLSelector().getText().setText(model.getPath());
                        model.getEditNode().getVideoSizeSelector().getDropdown().getSelectionModel().select(model.getSize());
                        newPage.addComponent(model);
                    }
                    else if("Slide Show".equals(type)){
                        SlideShowComponentModel model = new SlideShowComponentModel(component.getString("title"));
                        model.getEditNode().getTitleSelector().getText().setText(model.getTitle());
                        for(int k = 0; k < component.getJsonArray("slides").size(); k++){
                            JsonObject slideJsonObject = component.getJsonArray("slides").getJsonObject(k);
                            SlideModel slideToAdd = new SlideModel(slideJsonObject.getString("imagePath"), slideJsonObject.getString("caption"),model);
                            model.addSlide(slideToAdd);
                        }
                        newPage.addComponent(model);
                    }
                    else if("Textfield".equals(type)){
                        TextComponentModel model = new TextComponentModel(component.getString("title"), component.getString("text"));
                        model.setFont(component.getInt("font"));
                        model.getEditNode().getTitleSelector().getText().setText(model.getTitle());
                        model.getEditNode().getFontSelector().getDropdown().getSelectionModel().select(model.getFont());
                        model.getEditNode().getText().setText(model.getText());
                        for(int k = 0; k<component.getJsonArray("hyperlinks").size(); k++){
                            JsonObject hyperlinkJsonObject = component.getJsonArray("hyperlinks").getJsonObject(k);
                            model.addHyperlink(hyperlinkJsonObject.getString("url"), hyperlinkJsonObject.getString("text"), hyperlinkJsonObject.getInt("startIndex"), hyperlinkJsonObject.getInt("endIndex"));
                        }
                        newPage.addComponent(model);
                    }
                }
            }
            site.addPageModel(newPage);
        }
        ui.getSiteToolBar().getSiteControlsPanel().getSiteTitleControls().getText().setText(site.getSiteTitle());
        
        
        return site;
    }
    
    private JsonObject loadJSONFile(String path) throws IOException{
        InputStream is = new FileInputStream(path);
        JsonReader jsonReader = Json.createReader(is);
        JsonObject json = jsonReader.readObject();
        jsonReader.close();
        return json;
    }
    
    public void exportSite(){
        try{
            saveSite();
            
            //First delete the old one if it exists
            File oldSite = new File("sites/" + site.getSiteTitle());
            if(oldSite!=null){
                FileUtils.deleteDirectory(oldSite);
            }
            
            //Create new site folder
            File from = new File("sites/base");
            File to = new File("sites/" + site.getSiteTitle());
            
            FileUtils.copyDirectory(from, to);
            
            //copy json file to new site folder
            from=new File("saves/" + site.getSiteTitle() + ".json");
            to=new File("sites/" + site.getSiteTitle() + "/"  + "site.json");
            FileUtils.copyFile(from, to);
            
            //create the html files for all pages
            for(PageModel p: site.getPages()){
                if(site.getPages().indexOf(p)!=0){
                    from = new File("sites/base/index.html");
                    to = new File("sites/" + site.getSiteTitle() + "/" + p.getFileName());
                    FileUtils.copyFile(from,to);
                }
            }
            
            //copy images to site folder
            for(PageModel p:site.getPages()){
                BannerComponentModel bM = p.getBanner();
                if(!(bM.getBannerImagePath().equalsIgnoreCase(""))){
                    from = new File(bM.getBannerImagePath());
                    to=new File("sites/" + site.getSiteTitle() + "/media/img/" + bM.getBannerImageFileName());
                    FileUtils.copyFile(from, to);
                }
                
                for(PageComponentModel m: p.getComponents()){
                    if(m.getType()=="Image"){
                        ImageComponentModel iM = (ImageComponentModel)m;
                        if(!(iM.getPath().equals(" "))){
                            from = new File(iM.getPath());
                            to=new File("sites/" + site.getSiteTitle() + "/media/img/" + iM.getFileName());
                            FileUtils.copyFile(from, to);
                        }
                    }
                    else if(m.getType()=="Video"){
                        VideoComponentModel vM = (VideoComponentModel)m;
                        if(!(vM.getPath().equals(" "))){
                            from = new File(vM.getPath());
                            to=new File("sites/" + site.getSiteTitle() + "/media/video/" + vM.getFileName());
                            FileUtils.copyFile(from, to);
                        }
                    }
                    else if(m.getType()=="Slide Show"){
                        SlideShowComponentModel ssM = (SlideShowComponentModel)m;
                        for(SlideModel sM: ssM.getSlides()){
                            from = new File(sM.getImagePath());
                            to=new File("sites/" + site.getSiteTitle() + "/media/img/slideShowImages/" + sM.getImageFileName());
                            FileUtils.copyFile(from, to);
                        }
                    }
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(FileController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void viewSite(Tab siteViewTab){
        try{
            saveSite();
            
            //First delete the old one if it exists
            File oldSite = new File("sites/temp");
            if(oldSite!=null){
                FileUtils.deleteDirectory(oldSite);
            }
            
            //Create a temporary site folder
            File from = new File("sites/base");
            File to = new File("sites/temp");
            FileUtils.copyDirectory(from, to);
           
            //copy json file to new site folder
            from=new File("saves/" + site.getSiteTitle() + ".json");
            to=new File("sites/temp/site.json");
            FileUtils.copyFile(from, to);
            
            //create the html files for all pages
            for(PageModel p: site.getPages()){
                if(site.getPages().indexOf(p)!=0){
                    from = new File("sites/base/index.html");
                    to = new File("sites/temp/" + p.getFileName());
                    FileUtils.copyFile(from,to);
                }
            }
            
            //copy images to site folder
            for(PageModel p:site.getPages()){
                BannerComponentModel bM = p.getBanner();
                if(!(bM.getBannerImagePath().equalsIgnoreCase(""))){
                    from = new File(bM.getBannerImagePath());
                    to=new File("sites/temp/media/img/" + bM.getBannerImageFileName());
                    FileUtils.copyFile(from, to);
                }
                
                for(PageComponentModel m: p.getComponents()){
                    if(m.getType()=="Image"){
                        ImageComponentModel iM = (ImageComponentModel)m;
                        if(!(iM.getPath().equals(" "))){
                            from = new File(iM.getPath());
                            to=new File("sites/temp/media/img/" + iM.getFileName());
                            FileUtils.copyFile(from, to);
                        }
                    }
                    else if(m.getType()=="Video"){
                        VideoComponentModel vM = (VideoComponentModel)m;
                        if(!(vM.getPath().equals(" "))){
                            from = new File(vM.getPath());
                            to=new File("sites/temp/media/video/" + vM.getFileName());
                            FileUtils.copyFile(from, to);
                        }
                    }
                    else if(m.getType()=="Slide Show"){
                        SlideShowComponentModel ssM = (SlideShowComponentModel)m;
                        for(SlideModel sM: ssM.getSlides()){
                            from = new File(sM.getImagePath());
                            to=new File("sites/temp/media/img/slideShowImages/" + sM.getImageFileName());
                            FileUtils.copyFile(from, to);
                        }
                    }
                }
            }
            
        }catch(IOException ex){
            Logger.getLogger(FileController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        WebView webview = new WebView();
        
        File f = new File("sites/temp/index.html");
        
        webview.getEngine().load(f.toURI().toString());

        siteViewTab.setContent(webview);
    }
}
