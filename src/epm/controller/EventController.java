/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epm.controller;

import SiteModel.PageModel;
import SiteModel.SiteModel;
import epm.PageComponentEditNodes.PageComponentEditNode;
import epm.PageComponentModel.ImageComponentModel;
import epm.PageComponentModel.ListComponentModel;
import epm.PageComponentModel.PageComponentModel;
import epm.PageComponentModel.SlideShowComponentModel;
import epm.PageComponentModel.TextComponentModel;
import epm.PageComponentModel.VideoComponentModel;
import epm.toolbar.AddComponents;
import epm.toolbar.ComponentControls;
import epm.toolbar.PageControls;
import epm.toolbar.SiteToolBar;
import epm.view.ComponentEditView;
import epm.view.PageTab;
import epm.view.PageTabPane;
import epm.view.SiteEditView;
import static eportfoliomaker.StartupConstants.SITE_SAVES_PATH;
import java.io.File;
import java.io.IOException;
import java.util.Optional;
import javafx.application.Platform;
import javafx.scene.control.Alert;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Tab;
import javafx.stage.FileChooser;
import org.apache.commons.io.FileUtils;

/**
 *
 * @author briankondracki
 */
public class EventController {
    private SiteEditView ui;
    private SiteModel siteModel;
    private static EventController singleton = null;
    private FileController fileController;
    
    private EventController(SiteEditView ui, SiteModel siteModel){
        this.ui = ui;
        this.siteModel = siteModel;
        fileController = new FileController(siteModel);
    }
    
    public static EventController getInstance(SiteEditView ui, SiteModel siteModel){
        if(singleton == null)
            singleton = new EventController(ui, siteModel);
        
        return singleton;
    }
    
    public static EventController getInstance(){
        return singleton;
    }
    
    public void setSite(SiteModel initSite){
        this.siteModel = initSite;
    }
    
    public void handleAddPageRequest(){
        PageModel newPage = new PageModel("New Page", "NewPage.html");
        siteModel.addPage(newPage, ui);
        //getPageTabPane().addPage(newPage);
        getPageControlsPanel().getPageTitleControls().getText().setPromptText(newPage.getTitle());
        getPageControlsPanel().getPageTitleControls().getText().setDisable(false);
    }
    
    public void handleRemovePageRequest(){
        siteModel.removePage(getCurrentPageIndex(), ui);
    }
    
    public void handleAddTextComponent(){
        getCurrentPageModel().addComponent(new TextComponentModel("TextField", "Text"));
        //getComponentControlsPanel().getRemoveButton().setDisable(false);
    }
    
    public void handleAddImageComponent(){
        getCurrentPageModel().addComponent(new ImageComponentModel("Image"));
        getComponentControlsPanel().getRemoveButton().setDisable(false);
    }
    
    public void handleAddListComponent(){
        getCurrentPageModel().addComponent(new ListComponentModel("List"));
        getComponentControlsPanel().getRemoveButton().setDisable(false);
    }
    
    public void handleAddVideoComponent(){
        getCurrentPageModel().addComponent(new VideoComponentModel("Video"));
        getComponentControlsPanel().getRemoveButton().setDisable(false);
    }
    
    public void handleAddSlideShowComponent(){
        getCurrentPageModel().addComponent(new SlideShowComponentModel("Slide Show"));
        getComponentControlsPanel().getRemoveButton().setDisable(false);
    }
    
    public void handlePageTitleChanged(String title){
        getCurrentPageModel().setTitle(title);
        getCurrentPageTab().setText(title);
    }
    
    public void handleTabClicked(){
        if(!(getSite().getPages().isEmpty())){
            getPageControlsPanel().getAddRemoveControls().getRight().setDisable(false);
            getPageControlsPanel().setTitleControlText(getCurrentPageModel().getTitle());
            getPageControlsPanel().getPageTemplateControls().getDropdown().setDisable(false);
            getPageControlsPanel().getPageColorSchemeControls().getDropdown().setDisable(false);
            getPageControlsPanel().setColorSchemeDropdownOption(getCurrentPageModel().getColorScheme()-1);
            getPageControlsPanel().getPageFontControls().getDropdown().setDisable(false);
            getPageControlsPanel().setTemplateDropdownOption(getCurrentPageModel().getTemplate()-1);
            getPageControlsPanel().setFontDropdownOption(getCurrentPageModel().getFont()-1);
            getPageControlsPanel().getPageTitleControls().getText().setDisable(false);
            getAddComponentsPanel().getNewText().setDisable(false);
            getAddComponentsPanel().getNewList().setDisable(false);
            getAddComponentsPanel().getNewImage().setDisable(false);
            getAddComponentsPanel().getNewVideo().setDisable(false);
            getAddComponentsPanel().getNewSlideShow().setDisable(false);
            if(getCurrentPageModel().getComponents().size()>0)
                getComponentControlsPanel().getRemoveButton().setDisable(false);
        }
        else{
            getPageControlsPanel().getAddRemoveControls().getRight().setDisable(true);
            getPageControlsPanel().getPageTemplateControls().getDropdown().setDisable(true);
            getPageControlsPanel().getPageColorSchemeControls().getDropdown().setDisable(true);
            getPageControlsPanel().getPageFontControls().getDropdown().setDisable(true);
            getPageControlsPanel().getPageTitleControls().getText().setDisable(true);
            getAddComponentsPanel().getNewText().setDisable(true);
            getAddComponentsPanel().getNewList().setDisable(true);
            getAddComponentsPanel().getNewImage().setDisable(true);
            getAddComponentsPanel().getNewVideo().setDisable(true);
            getAddComponentsPanel().getNewSlideShow().setDisable(true);
        }
    }
    
    public void handleSaveRequest(){
        try{
            fileController.saveSite();
        }catch(Exception e){
            
        }
    }
    
    private boolean promptToSave(){
        Alert alert = new Alert(Alert.AlertType.CONFIRMATION);
        alert.setTitle(siteModel.getSiteTitle());
        alert.setContentText("Would you like to save the current site before continuing?");
        alert.setHeaderText(null);
        Optional<ButtonType> result = alert.showAndWait();
        
        return result.get() == ButtonType.OK;
    }
    
    public void handleLoadRequest(){
        promptToOpen();
    }
    
    public void handleNewSiteRequest(){
        if(promptToSave())
            handleSaveRequest();
        siteModel = new SiteModel();
        getPageTabPane().getTabs().clear();
        fileController.setSite(siteModel);
    }
    
    public void handleExportSiteRequest(){
        fileController.exportSite();
    }
    
    public void handleCloseApplicationRequest(){
        try{
            FileUtils.deleteDirectory(new File("sites/temp"));
        }catch(IOException ex){
            System.out.println("Temp doesnt exist");
        }
        Platform.exit();
    }
    
    private void promptToOpen(){
        FileChooser siteFileChooser = new FileChooser();
        siteFileChooser.setInitialDirectory(new File(SITE_SAVES_PATH));
        File selectedFile = siteFileChooser.showOpenDialog(null);
        
        if(selectedFile != null){
            try{
                getPageTabPane().getTabs().clear();
                siteModel =fileController.loadSite(selectedFile.getAbsolutePath(), ui);
                siteModel.loadPagesToScreen(ui);
            }catch(IOException exx){
                System.out.println("IO ERROR NOT FOUND FILE");
            }
        }
    }
    
    public void handleSiteViewRequest(Tab siteViewTab){
        fileController.viewSite(siteViewTab);
    }
    
    public void handleComponentClicked(PageComponentEditNode n, PageComponentModel m){
        int index = getCurrentPageModel().getComponents().indexOf(m);
        if(getCurrentPageModel().getSelectedIndex()!=-1){
            getCurrentPageModel().getComponents().get(getCurrentPageModel().getSelectedIndex()).getEditNode().getStyleClass().remove("page_component_edit_node_highlighted");
        }
        getCurrentPageModel().setSelectedIndex(index);
        getCurrentPageModel().getComponents().get(getCurrentPageModel().getSelectedIndex()).getEditNode().getStyleClass().add("page_component_edit_node_highlighted");
        getComponentControlsPanel().getRemoveButton().setDisable(false);
    }
    
    public void handleRemoveComponent(){
        if(getCurrentPageModel().getSelectedIndex()==-1)
            return;
        else{
            getCurrentPageModel().removeComponent(getCurrentPageModel().getComponents().get(getCurrentPageModel().getSelectedIndex()));
            getCurrentPageModel().setSelectedIndex(-1);
        }
    }
    
    public SiteModel getSite(){ return siteModel; }
    public SiteEditView getSiteEditView(){ return ui; }
    public SiteToolBar getSiteToolBar(){ return ui.getSiteToolBar(); }
    public PageControls getPageControlsPanel(){ return getSiteToolBar().getPageControlsPanel(); }
    public ComponentControls getComponentControlsPanel(){ return getSiteToolBar().getComponentControlsPanel(); }
    public AddComponents getAddComponentsPanel(){ return getSiteToolBar().getAddComponentsPanel(); }
    
    public PageModel getCurrentPageModel(){ 
        return siteModel.getPages().get(getCurrentPageIndex()); 
    }
    public PageTabPane getPageTabPane(){ return ui.getPageTabPane(); } 
    public PageTab getCurrentPageTab(){ return getPageTabPane().getCurrentTab(); }
    public int getCurrentPageIndex() { return getPageTabPane().getCurrentPageIndex(); }
    public ComponentEditView getComponentEditView(){ return getCurrentPageTab().getComponentEditView(); }
}
