/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epm.toolbar;

import epm.InputOptions.DualButtons;
import epm.controller.EventController;
import static eportfoliomaker.StartupConstants.CSS_CLASS_TOOLBAR_BUTTONS;
import static eportfoliomaker.StartupConstants.CSS_CLASS_TOOLBAR_PANEL;
import static eportfoliomaker.StartupConstants.CSS_CLASS_TOOLBAR_TITLE;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;

/**
 *
 * @author briankondracki
 */
public class ComponentControls extends VBox{
    private Label title;
    private Button removeButton;
    EventController controller = EventController.getInstance();
    
    public ComponentControls(){
        title = new Label("Component Controls:");
        
        removeButton = new Button("Remove");
        removeButton.setDisable(true);
        removeButton.setAlignment(Pos.CENTER);
        
        getChildren().addAll(title,removeButton);
        
        initEventHandlers();
        
        // CSS CLASS
        getStyleClass().add(CSS_CLASS_TOOLBAR_PANEL);
        getStyleClass().add(CSS_CLASS_TOOLBAR_BUTTONS);
        title.getStyleClass().add(CSS_CLASS_TOOLBAR_TITLE);
        removeButton.getStyleClass().add("toolbar_buttons");
    }
    
    public Button getRemoveButton(){ return removeButton; }
    
    public void initEventHandlers(){
        removeButton.setOnAction(e->{
            controller.handleRemoveComponent();
        });
    }
}
