/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epm.toolbar;

import epm.InputOptions.LabelTextFieldOption;
import epm.controller.EventController;
import static eportfoliomaker.StartupConstants.CSS_CLASS_TOOLBAR_PANEL;
import static eportfoliomaker.StartupConstants.CSS_CLASS_TOOLBAR_TITLE;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;

/**
 *
 * @author Brian
 */
public class SiteControls extends VBox{
    Label title;
    private LabelTextFieldOption siteTitle;
    EventController controller;
    
    public SiteControls(){
        title = new Label("Site Controls");
        siteTitle = new LabelTextFieldOption("Site Title");
        
        getChildren().addAll(title,siteTitle);
        
        controller = EventController.getInstance();
        
        initEventHandlers();
        
        // CSS CLASS
        getStyleClass().add(CSS_CLASS_TOOLBAR_PANEL);
        siteTitle.getStyleClass().add("page_control_input");
        title.getStyleClass().add(CSS_CLASS_TOOLBAR_TITLE);
    }
    
    public LabelTextFieldOption getSiteTitleControls(){ return siteTitle; }
    
    private void initEventHandlers(){
        siteTitle.getText().textProperty().addListener(e->{
            controller.getSite().setSiteTitle(siteTitle.getText().getText());
        });
    }
}
