/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epm.toolbar;

import static eportfoliomaker.StartupConstants.CSS_CLASS_SITE_TOOLBAR;
import javafx.geometry.Rectangle2D;
import javafx.scene.layout.VBox;
import javafx.stage.Screen;

/**
 *
 * @author Brian
 */
public class SiteToolBar extends VBox{
    private SiteControls siteControlsPanel;
    private PageControls pageControlsPanel;
    private ComponentControls componentControlsPanel;
    private AddComponents addComponentsPanel;
    
    public SiteToolBar(){
        siteControlsPanel = new SiteControls();
        pageControlsPanel = new PageControls();
        componentControlsPanel = new ComponentControls();
        addComponentsPanel = new AddComponents();
        
        getChildren().addAll(siteControlsPanel, pageControlsPanel, componentControlsPanel, addComponentsPanel);
        
        // GET THE SIZE OF THE SCREEN
        Screen screen = Screen.getPrimary();
        Rectangle2D bounds = screen.getVisualBounds();
        
        setPrefHeight(bounds.getHeight());
        
        // CSS CLASS
        getStyleClass().add(CSS_CLASS_SITE_TOOLBAR);
    }
    
    public SiteControls getSiteControlsPanel(){ return siteControlsPanel; }
    public PageControls getPageControlsPanel(){ return pageControlsPanel; }
    public ComponentControls getComponentControlsPanel(){ return componentControlsPanel; }
    public AddComponents getAddComponentsPanel(){ return addComponentsPanel; }
}
