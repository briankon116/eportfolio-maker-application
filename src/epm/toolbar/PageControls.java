/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epm.toolbar;

import epm.InputOptions.DualButtons;
import epm.InputOptions.LabelDropdownOption;
import epm.InputOptions.LabelTextFieldOption;
import epm.controller.EventController;
import static eportfoliomaker.StartupConstants.CSS_CLASS_TOOLBAR_PANEL;
import static eportfoliomaker.StartupConstants.CSS_CLASS_TOOLBAR_TITLE;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;

/**
 *
 * @author bkondracki
 */
public class PageControls extends VBox{
    private Label title;
    private LabelTextFieldOption pageTitle;
    private LabelDropdownOption pageTemplate;
    private LabelDropdownOption pageColorScheme;
    private LabelDropdownOption pageFont;
    private DualButtons addRemoveButton;
    private EventController controller;
    
    public PageControls(){
        title = new Label("Page Controls:");
        
        pageTitle = new LabelTextFieldOption("Page Title:");
        pageTitle.getText().setDisable(true);
        
        pageTemplate = new LabelDropdownOption("Page Template");
        pageTemplate.getDropdown().getItems().addAll("Template 1", "Template 2", "Template 3", "Template 4", "Template 5");
        pageTemplate.getDropdown().setPromptText("Select a Template");
        pageTemplate.getDropdown().setDisable(true);
        
        pageColorScheme = new LabelDropdownOption("Page Color Scheme");
        pageColorScheme.getDropdown().getItems().addAll("Color Scheme 1", "Color Scheme 2", "Color Scheme 3", "Color Scheme 4", "Color Scheme 5");
        pageColorScheme.getDropdown().setPromptText("Select a Color Scheme");
        pageColorScheme.getDropdown().setDisable(true);
        
        pageFont = new LabelDropdownOption("Page Font");
        pageFont.getDropdown().getItems().addAll("Bitter", "Montserrat", "Josefin Slab", "Lato", "Oswald");
        pageFont.getDropdown().setPromptText("Select a Font");
        pageFont.getDropdown().setDisable(true);
        
        addRemoveButton = new DualButtons("Add","Remove");
        addRemoveButton.setDisableAll();
        addRemoveButton.getChildren().get(0).setDisable(false);
        
        getChildren().addAll(title,pageTitle,pageTemplate, pageColorScheme, pageFont, addRemoveButton);
        
        controller = EventController.getInstance();
        
        initEventHandlers();
        
        // CSS CLASS
        getStyleClass().add(CSS_CLASS_TOOLBAR_PANEL);
        pageTitle.getStyleClass().add("page_control_input");
        pageTemplate.getStyleClass().add("page_control_input");
        pageColorScheme.getStyleClass().add("page_control_input");
        pageFont.getStyleClass().add("page_control_input");
        title.getStyleClass().add(CSS_CLASS_TOOLBAR_TITLE);
    }
    ///chage later
    public LabelTextFieldOption getPageTitleControls(){ return pageTitle; }
    public LabelDropdownOption getPageTemplateControls(){ return pageTemplate; }
    public LabelDropdownOption getPageColorSchemeControls(){ return pageColorScheme; }
    public LabelDropdownOption getPageFontControls(){ return pageFont; }
    public DualButtons getAddRemoveControls(){ return addRemoveButton; }
    
    public void setTitleControlText(String title){
        pageTitle.getText().setText(title);
    }
    
    public void setTemplateDropdownOption(int index){
        pageTemplate.getDropdown().getSelectionModel().select(index);
    }
    
    public void setColorSchemeDropdownOption(int index){
        pageColorScheme.getDropdown().getSelectionModel().select(index);
    }
    
    public void setFontDropdownOption(int index){
        pageFont.getDropdown().getSelectionModel().select(index);
    }
    
    public void initEventHandlers(){
        pageTitle.getText().textProperty().addListener(e->{
            controller.handlePageTitleChanged(pageTitle.getText().getText());
        });
        
        pageTemplate.getDropdown().setOnAction(e->{
            controller.getCurrentPageModel().setTemplate(pageTemplate.getDropdown().getSelectionModel().getSelectedIndex()+1);
        });
        
        pageColorScheme.getDropdown().setOnAction(e->{
           controller.getCurrentPageModel().setColorScheme(pageColorScheme.getDropdown().getSelectionModel().getSelectedIndex()+1);
        });
        
        pageFont.getDropdown().setOnAction(e->{
            controller.getCurrentPageModel().setFont(pageFont.getDropdown().getSelectionModel().getSelectedIndex()+1);
        });
        
        addRemoveButton.getLeft().setOnMouseClicked(e->{
            controller.handleAddPageRequest();
        });
        
        addRemoveButton.getRight().setOnMouseClicked(e->{
            controller.handleRemovePageRequest();
        });
    }
}
