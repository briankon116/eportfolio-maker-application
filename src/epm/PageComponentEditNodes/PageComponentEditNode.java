/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epm.PageComponentEditNodes;

import epm.controller.EventController;
import eportfoliomaker.ScreenSizeSingleton;
import static eportfoliomaker.StartupConstants.CSS_CLASS_PAGE_COMPONENT_EDIT_NODE;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;


/**
 *
 * @author briankondracki
 */
public class PageComponentEditNode extends VBox{
    /**
     * @param title This is the title/type of page component. All child components will be identified by this title.
     */
    private String title;
    private boolean isSelected;
    private Label componentTitleLabel;
    private EventController controller;
    
    /**
     * Default constructor for the PageComponent object. Sets the title to an empty string and isSelected to false.
     */
    public PageComponentEditNode(){
        title = "";
        isSelected = false;
        componentTitleLabel = new Label();
        
        //Set the size
        ScreenSizeSingleton screenSize = ScreenSizeSingleton.getInstance();
        setPrefWidth(screenSize.getWidth()*.5);
        
        //Set spacing
        setSpacing(3);
        
        //Initialize controller
        controller = EventController.getInstance();
        
        //Set CSS class
        getStyleClass().add(CSS_CLASS_PAGE_COMPONENT_EDIT_NODE);
    }
    
    /**
     * Overloaded constructor for the PageComponent object. Sets the title and isSelected variables to specified values.
     * @param title The title/type of PageComponent.
     * @param isSelected Boolean that tells if the object is selected or not.
     */
    public PageComponentEditNode(String title, boolean isSelected){
        this.title = title;
        this.isSelected = isSelected;
        componentTitleLabel = new Label(title);
        
        //Set the size
        ScreenSizeSingleton screenSize = ScreenSizeSingleton.getInstance();
        setPrefWidth(screenSize.getWidth()*.5);
        
        //Set spacing
        setSpacing(3);
        
        //Initialize controller
        controller = EventController.getInstance();
        
        //Set CSS class
        getStyleClass().add(CSS_CLASS_PAGE_COMPONENT_EDIT_NODE);
    }
    
    /**
     * @return The title of the PageComponent object.
     */
    public String getTitle(){
        return title;
    }
    
    /**
     * @return A boolean telling if the object is selected or not. 
     */
    public boolean getIsSelected(){
        return isSelected;
    }
    
    public Label getComponentTitleLabel(){
        return componentTitleLabel;
    }
    
    public EventController getController(){ return controller; }
    
    /**
     * Sets a new title for the PageComponet object.
     * @param initTitle The new title for the PageComponent object.
     */
    public void setTitle(String initTitle){
        title = initTitle;
    }
    
    /**
     * Sets a new value for the isSelected variable.
     * @param initIsSelected A boolean telling if the object is selected or not.
     */
    public void setIsSelected(boolean initIsSelected){
        isSelected = initIsSelected;
    }
    
    public void setComponentTitleLabel(String labelText){
        componentTitleLabel.setText(labelText);
    }
}
