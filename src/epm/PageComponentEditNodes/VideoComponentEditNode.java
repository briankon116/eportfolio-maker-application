/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epm.PageComponentEditNodes;

import epm.InputOptions.LabelDropdownOption;
import epm.InputOptions.LabelTextFieldOption;
import epm.InputOptions.TextFieldButtonOption;
import epm.PageComponentModel.VideoComponentModel;
import epm.controller.EventController;
import epm.controller.MediaSelectionController;
import java.util.ArrayList;

/**
 *
 * @author briankondracki
 */
public class VideoComponentEditNode extends PageComponentEditNode{
    /**
     * @param videoTitleSelector This is the LabelTextFieldOption object that allows the user to enter the title of the video.
     * @param URLSelector This is the TextFieldButtonOption that allows the user to select which video they want to include in
     * their webpage. They can either type in the URL in the searchbox or press the search button and select which video they want
     * using a GUI.
     * @parm videoSizeSelector This is the LabelDropdownOption object that allows the user to select what size they want their
     * video to appear as on their webpage. 
     */
    private LabelTextFieldOption titleSelector;
    private TextFieldButtonOption URLSelector;
    private LabelDropdownOption videoSizeSelector;
    private VideoComponentModel model;
    private MediaSelectionController videoSelector;
    EventController controller = EventController.getInstance();
    
    /**
     * This is the default constructor for the VideoComponentEditNode. This accepts no variables as initializes the dropdown menu
     * with no options.
     */
    public VideoComponentEditNode(VideoComponentModel model){
        super("Video", false);
        titleSelector = new LabelTextFieldOption("Video Title");
        URLSelector = new TextFieldButtonOption("Enter Video URL", "Search");
        videoSizeSelector = new LabelDropdownOption("Video Size");
        
        this.model = model;
        videoSelector = new MediaSelectionController();
        
        getChildren().addAll(getComponentTitleLabel(),titleSelector,URLSelector,videoSizeSelector);
        
        initEventHandlers();
    }
    
    /**
     * This is the overloaded constructor for the VideoComponentEditNode. This accepts on variable, videoSizes.
     * @param videoSizes This is an array list containing all of the video sizes available to the user.
     */
    public VideoComponentEditNode(ArrayList<String>videoSizes){
        super("Video",false);
        titleSelector = new LabelTextFieldOption("Video Title");
        URLSelector = new TextFieldButtonOption("Enter Video URL", "Search");
        videoSizeSelector = new LabelDropdownOption("Video Size", videoSizes);
        
        this.model = model;
        videoSelector = new MediaSelectionController();
        
        getChildren().addAll(getComponentTitleLabel(),titleSelector,URLSelector,videoSizeSelector);
        
        initEventHandlers();
    }
    
    public VideoComponentModel getModel(){ return model; }
    
    /**
     * @return The videoTitleSelector object. 
     */
    public LabelTextFieldOption getTitleSelector(){ return titleSelector; }
    
    /**
     * @return The URLSelector object. 
     */
    public TextFieldButtonOption getURLSelector(){ return URLSelector; }
    
    /**
     * @return The videoSizeSelector object. 
     */
    public LabelDropdownOption getVideoSizeSelector(){ return videoSizeSelector; }
    
    public void initEventHandlers(){
        titleSelector.getText().textProperty().addListener(e->{
            model.setTitle(titleSelector.getText().getText());
        });
        
        URLSelector.getText().textProperty().addListener(e->{
            model.setPath(URLSelector.getText().getText());
        });
        
        URLSelector.getButton().setOnAction(e->{
            videoSelector.processSelectVideo(this);
        });
        
        videoSizeSelector.getDropdown().setOnAction(e->{
            model.setSize(videoSizeSelector.getDropdown().getSelectionModel().getSelectedIndex());
        });
        
        this.setOnMouseClicked(e->{
            controller.handleComponentClicked(this, model);
        });
    }
}
