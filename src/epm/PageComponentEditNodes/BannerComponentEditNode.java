/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epm.PageComponentEditNodes;

import epm.InputOptions.LabelTextFieldOption;
import epm.InputOptions.TextFieldButtonOption;
import epm.PageComponentModel.BannerComponentModel;
import epm.controller.MediaSelectionController;
import static eportfoliomaker.StartupConstants.DEFAULT_THUMBNAIL_HEIGHT;
import static eportfoliomaker.StartupConstants.PATH_IMAGES;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 *
 * @author briankondracki
 */
public class BannerComponentEditNode extends PageComponentEditNode{
    private LabelTextFieldOption bannerText;
    private LabelTextFieldOption footerText;
    private TextFieldButtonOption setImageButton;
    private Image thumbnail;
    private ImageView thumbnailView;
    private BannerComponentModel model;
    private MediaSelectionController imageController;
    
    public BannerComponentEditNode(BannerComponentModel model){
        super("Banner", false);
        bannerText = new LabelTextFieldOption("Banner Text");
        footerText = new LabelTextFieldOption("Footer Text");
        setImageButton = new TextFieldButtonOption("Enter Banner Image URL", "Search");
        try{
            File file = new File(PATH_IMAGES + "NoImage.png");
            URL fileURL = file.toURI().toURL();

            thumbnail = new Image(fileURL.toExternalForm());
            thumbnailView = new ImageView(thumbnail);
        }catch(Exception e){
            System.out.println("No Image Found");
        }
        
        this.model = model;
        imageController = new MediaSelectionController();
        
        getChildren().addAll(getComponentTitleLabel(),bannerText, footerText, setImageButton, thumbnailView);
        
        initEventHandlers();
    }
    
    public BannerComponentModel getModel(){ return model; }
    public TextFieldButtonOption getSetImageButton(){ return setImageButton; }
    public LabelTextFieldOption getBannerText(){ return bannerText; }
    public LabelTextFieldOption getFooterText(){ return footerText; }
    
    public void setNewThumbnail(){
        try{
            File file = new File(model.getBannerImagePath());
            URL fileURL = file.toURI().toURL();
            
            thumbnail = new Image(fileURL.toExternalForm());
            thumbnailView.setImage(thumbnail);
            double scaledHeight = DEFAULT_THUMBNAIL_HEIGHT;
            double perc = scaledHeight / thumbnail.getHeight();
            double scaledWidth = thumbnail.getWidth()*perc;
            thumbnailView.setFitHeight(scaledHeight);
            thumbnailView.setFitWidth(scaledWidth);
        }catch(MalformedURLException e){
            System.out.println("Image not foune!!!");
            //SET ALERT FOR THIS EXCPEITON!!!
        }
    }
    
    public void initEventHandlers(){
        bannerText.getText().textProperty().addListener(e->{
            model.setBannerText(bannerText.getText().getText());
        });
        
        footerText.getText().textProperty().addListener(e->{
            model.setFooterText(footerText.getText().getText());
        });
        
        setImageButton.getButton().setOnAction(e->{
            imageController.processSelectImage(this);
        });
        
        setImageButton.getText().textProperty().addListener(e->{
            model.setBannerImagePath(setImageButton.getText().getText());
        });
    }
}
