/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epm.PageComponentEditNodes;

import epm.InputOptions.LabelTextFieldOption;
import epm.PageComponentModel.HyperlinkComponentModel;
import static eportfoliomaker.StartupConstants.CSS_CLASS_TOOLBAR_BUTTONS;
import static eportfoliomaker.StartupConstants.CSS_CLASS_TOOLBAR_PANEL;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

/**
 *
 * @author briankondracki
 */
public class HyperlinkComponentEditNode extends Stage{
    private VBox container;
    private Button removeHyperlink;
    private Label hyperlinkText;
    private LabelTextFieldOption url;
    private Button okButton;
    private Scene primaryScene;
    private HyperlinkComponentModel model;
    
    public HyperlinkComponentEditNode(String initText, HyperlinkComponentModel model){
        container = new VBox();
        
        removeHyperlink = new Button("Remove Hyperlink");
        hyperlinkText = new Label("Hyperlink Text: " + initText);
        hyperlinkText.setWrapText(true);
        url = new LabelTextFieldOption("Hyperlink URL");
        url.getText().setPrefWidth(300);
        okButton = new Button("Ok");
        
        //Allignment
        hyperlinkText.setPadding(new Insets(5,5,0,5));
        url.setPadding(new Insets(5,5,0,5));
        okButton.setAlignment(Pos.CENTER);
        
        initEventHandlers();
        
        this.model = model;
        
        container.getChildren().addAll(hyperlinkText,url,removeHyperlink,okButton);
        
        container.setSpacing(10);
        
        primaryScene = new Scene(container, 420,200);
        setScene(primaryScene);
        this.setResizable(false);
        
        //getStyleClass().add(CSS_CLASS_TOOLBAR_PANEL);
        okButton.getStyleClass().add(CSS_CLASS_TOOLBAR_BUTTONS);
    }
    
    public HyperlinkComponentEditNode(String initText,String initURL){
        container = new VBox();
        
        removeHyperlink = new Button("Remove Hyperlink");
        hyperlinkText = new Label("Hyperlink Text: " + initText);
        hyperlinkText.getStyleClass().add("hyperlink_window_components");
        url = new LabelTextFieldOption("Hyperlink URL");
        okButton = new Button("Ok");
        
        initEventHandlers();
        
        container.getChildren().addAll(removeHyperlink,hyperlinkText,url,okButton);
        
        primaryScene = new Scene(container, 400,200);
        setScene(primaryScene);
    }
    
    public void initEventHandlers(){
        removeHyperlink.setOnAction(e->{
            int index = model.getTextModel().getEditNode().getHyperlinkViewer().getDropdown().getSelectionModel().getSelectedIndex();
            model.getTextModel().getEditNode().getHyperlinkViewer().getDropdown().getItems().remove(index);
            model.getTextModel().getHyperlinks().remove(model);
            this.close();
        });
        
        url.getText().textProperty().addListener(e->{
            model.setURL(url.getText().getText());
        });
        
        okButton.setOnAction(e->{
           this.close();
        });
    }
    
    public void showEditNode(){
        showAndWait();
    }
}
