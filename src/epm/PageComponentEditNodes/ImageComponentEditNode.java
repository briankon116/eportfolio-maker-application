/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epm.PageComponentEditNodes;

import epm.InputOptions.LabelDropdownOption;
import epm.InputOptions.LabelTextFieldOption;
import epm.InputOptions.TextFieldButtonOption;
import epm.PageComponentModel.ImageComponentModel;
import epm.controller.EventController;
import epm.controller.MediaSelectionController;
import static eportfoliomaker.StartupConstants.DEFAULT_THUMBNAIL_HEIGHT;
import static eportfoliomaker.StartupConstants.PATH_IMAGES;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 *
 * @author Brian Kondracki
 */
public class ImageComponentEditNode extends PageComponentEditNode{
    /**
     * @param imageTitleSelector The LabelTextFieldOption object that allows the user to enter the title of the image. This title
     * will be displayed above the image as a header on the webpage.
     * @param setImageButton The TextFieldButtonOption object that allows the user to specify the location of the image they would
     * like to set on the webpage. When the button is pressed an imageselection window is opened so the user can search through 
     * their hard drive to find the image.
     * @param thumbnail The image object of the image to be displayed on the webpage.
     * @param thumbnailView The ImageView object that lets the user view a preview of the image that they want to display on the 
     * webpage before it is generated.
     */
    private LabelTextFieldOption titleSelector;
    private TextFieldButtonOption setImageButton;
    private Image thumbnail;
    private ImageView thumbnailView;
    private ImageComponentModel model;
    private MediaSelectionController imageController;
    private LabelDropdownOption floatImageSelector;
    EventController controller = EventController.getInstance();
    
    /**
     * The only constructor for the ImageComponentEditNode object. There are no input variables since each object of this type will
     * have the same starting values.
     */
    public ImageComponentEditNode(ImageComponentModel model){
        super("Image", false);
        titleSelector = new LabelTextFieldOption("Image Title");
        floatImageSelector = new LabelDropdownOption("Float Image");
        floatImageSelector.getDropdown().getItems().addAll("Neither", "Left", "Right");
        floatImageSelector.getDropdown().getSelectionModel().select((0));
        setImageButton = new TextFieldButtonOption("Enter Image URL", "Search");
        try{
            File file = new File(PATH_IMAGES + "NoImage.png");
            URL fileURL = file.toURI().toURL();
            
            thumbnail = new Image(fileURL.toExternalForm());
            thumbnailView = new ImageView();
            thumbnailView.setImage(thumbnail);
        }catch(Exception e){
            System.out.println("No image found!!!");
        }
        
        imageController = new MediaSelectionController();
        
        this.model = model;
        
        initEventHandlers();
        
        getChildren().addAll(getComponentTitleLabel(),titleSelector,floatImageSelector,setImageButton, thumbnailView);
    }
    
    public ImageComponentModel getModel(){ return model; }
    
    /**
     * @return The ImageTitleSelector object. 
     */
    public LabelTextFieldOption getTitleSelector(){ return titleSelector; }
    
    /**
     * @return The setImageButton object. 
     */
    public TextFieldButtonOption getSetImageButton(){ return setImageButton; }
    
    /**
     * @return The Image object for the thumbnail.
     */
    public Image getThumbnail(){ return thumbnail; }
    
    public LabelDropdownOption getFloatImageSelector(){ return floatImageSelector; }
    
    /**
     * @return The ImageView object for the thumbnail. 
     */
    public ImageView getThumbnailView(){ return thumbnailView; }
    
    public void setNewThumbnail(){
        try{
            File file = new File(model.getPath());
            URL fileURL = file.toURI().toURL();
            
            thumbnail = new Image(fileURL.toExternalForm());
            thumbnailView.setImage(thumbnail);
            double scaledHeight = DEFAULT_THUMBNAIL_HEIGHT;
            double perc = scaledHeight / thumbnail.getHeight();
            double scaledWidth = thumbnail.getWidth()*perc;
            thumbnailView.setFitHeight(scaledHeight);
            thumbnailView.setFitWidth(scaledWidth);
        }catch(MalformedURLException e){
            System.out.println("Image not foune!!!");
            //SET ALERT FOR THIS EXCPEITON!!!
        }
    }
    
    public void reload(){
        getChildren().clear();
        getChildren().addAll(getComponentTitleLabel(),titleSelector,setImageButton, thumbnailView);
    }
    
    /**
     * Initializes the eventhandlers to be used in this object.
     */
    public void initEventHandlers(){
        titleSelector.getText().textProperty().addListener(e->{
            model.setTitle(titleSelector.getText().getText());
        });
        
        setImageButton.getText().textProperty().addListener(e->{
            model.setPath(setImageButton.getText().getText());
            setNewThumbnail();
        });
        
        setImageButton.getButton().setOnAction(e->{
            imageController.processSelectImage(this);
        });
        
        this.setOnMouseClicked(e->{
            controller.handleComponentClicked(this, model);
        });
        
        floatImageSelector.getDropdown().setOnAction(e->{
            model.setFloatImage(floatImageSelector.getDropdown().getSelectionModel().getSelectedIndex());
        });
    }
}
