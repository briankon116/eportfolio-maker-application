/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epm.PageComponentEditNodes;

import epm.InputOptions.DualButtons;
import epm.PageComponentModel.SlideModel;
import epm.PageComponentModel.SlideShowComponentModel;
import epm.controller.MediaSelectionController;
import static eportfoliomaker.StartupConstants.DEFAULT_THUMBNAIL_HEIGHT;
import static eportfoliomaker.StartupConstants.PATH_IMAGES;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;

/**
 *
 * @author briankondracki
 */
public class SlideEditor extends VBox{
    private Image thumbnail;
    private ImageView thumbnailView;
    private MediaSelectionController imageController;
    private TextField caption;
    private DualButtons leftRemoveRight;
    private SlideShowComponentModel model;
    private SlideModel slideModel;
    
    public SlideEditor(SlideShowComponentModel model, SlideModel slideModel){
        try{
            File file = new File(slideModel.getImagePath());
            URL fileURL = file.toURI().toURL();

            thumbnail = new Image(fileURL.toExternalForm());
            thumbnailView = new ImageView();
            thumbnailView.setImage(thumbnail);
            double scaledHeight = DEFAULT_THUMBNAIL_HEIGHT;
            double perc = scaledHeight / thumbnail.getHeight();
            double scaledWidth = thumbnail.getWidth()*perc;
            thumbnailView.setFitHeight(scaledHeight);
            thumbnailView.setFitWidth(scaledWidth);
        }catch(Exception e){
            System.out.println("Slide show error");
        }
        caption = new TextField();
        caption.setText(slideModel.getCaption());
            
        this.model = model;
        this.slideModel = slideModel;
        imageController = new MediaSelectionController();
        leftRemoveRight = new DualButtons("<-","X");
        leftRemoveRight.addButton("->", true);
        
        initEventHandlers();
        
        getChildren().addAll(thumbnailView,caption,leftRemoveRight);
    }
    
    public void setImage(){
        try{
            File file = new File(slideModel.getImagePath());
            URL fileURL = file.toURI().toURL();
            
            thumbnail = new Image(fileURL.toExternalForm());
            thumbnailView.setImage(thumbnail);
            double scaledHeight = DEFAULT_THUMBNAIL_HEIGHT;
            double perc = scaledHeight / thumbnail.getHeight();
            double scaledWidth = thumbnail.getWidth()*perc;
            thumbnailView.setFitHeight(scaledHeight);
            thumbnailView.setFitWidth(scaledWidth);
        }catch(MalformedURLException e){
            System.out.println("Image not found!!!");
            //SET ALERT FOR THIS EXCPEITON!!!
        }
    }
    
    public void initEventHandlers(){
        thumbnailView.setOnMouseClicked(e->{
            imageController.processSelectImage(slideModel);
        });
        
        caption.textProperty().addListener(e->{
            int index = model.getEditNode().getSlidesEditor().getSlides().indexOf(this);
            model.getSlides().get(index).setCaption(caption.getText());
        });
        
        leftRemoveRight.getLeft().setOnAction(e->{
            model.moveSlideLeft(model.getEditNode().getSlidesEditor().getSlides().indexOf(this));
        });
        
        leftRemoveRight.getRight().setOnAction(e->{
           int index = model.getEditNode().getSlidesEditor().getSlides().indexOf(this);
           model.removeSlide(index);
        });
        
        leftRemoveRight.getButton(2).setOnAction(e->{
            model.moveSlideRight(model.getEditNode().getSlidesEditor().getSlides().indexOf(this));
        });
    }
}
