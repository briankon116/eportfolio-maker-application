/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epm.PageComponentEditNodes;

import epm.InputOptions.LabelDropdownOption;
import epm.InputOptions.LabelTextFieldOption;
import epm.PageComponentModel.ListComponentModel;
import epm.controller.EventController;
import javafx.scene.control.Button;

/**
 *
 * @author briankondracki
 */
public class ListComponentEditNode extends PageComponentEditNode{
    private LabelTextFieldOption titleSelector;
    private LabelDropdownOption fontSelector;
    private Button addListItemButton;
    private ListItemsScrollPane listItems;
    private ListComponentModel model;
    EventController controller = EventController.getInstance();
    
    public ListComponentEditNode(ListComponentModel model){
        super("List",false);
        titleSelector = new LabelTextFieldOption("Header:");
        fontSelector = new LabelDropdownOption("Fonts");
        fontSelector.getDropdown().getItems().addAll("Bitter", "Montserrat", "Josefin Slab", "Lato", "Oswald");
        fontSelector.getDropdown().getSelectionModel().select(0);
        addListItemButton = new Button("Add List Item");
        this.model = model;
        listItems = new ListItemsScrollPane(model);
        
        getChildren().addAll(getComponentTitleLabel(), titleSelector,fontSelector,addListItemButton, listItems);
        
        initEventHandlers();
    }
    
    public ListItemsScrollPane getListItems(){ return listItems; }
    public LabelTextFieldOption getTitleSelector(){ return titleSelector; }
    public LabelDropdownOption getFontSelector(){ return fontSelector; }
    
    private void initEventHandlers(){
        titleSelector.getText().textProperty().addListener(e->{
            model.setTitle(titleSelector.getText().getText());
        });
        
        fontSelector.getDropdown().setOnAction(e->{
           model.setFont(fontSelector.getDropdown().getSelectionModel().getSelectedIndex()); 
        });
        
        addListItemButton.setOnAction(e->{
           model.addListItem(""); 
        });
        
        this.setOnMouseClicked(e->{
            controller.handleComponentClicked(this, model);
        });
    }
}
