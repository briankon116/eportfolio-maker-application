/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epm.PageComponentEditNodes;

import epm.InputOptions.LabelTextFieldOption;
import epm.PageComponentModel.SlideShowComponentModel;
import epm.controller.EventController;
import static eportfoliomaker.StartupConstants.PATH_IMAGES;
import javafx.scene.control.Button;


/**
 *
 * @author briankondracki
 */
public class SlideShowComponentEditNode extends PageComponentEditNode{
    private LabelTextFieldOption titleSelector;
    private Button addButton;
    private SlidesEditor slides;
    private SlideShowComponentModel model;
    EventController controller = EventController.getInstance();
    
    public SlideShowComponentEditNode(SlideShowComponentModel model){
        super("Slide Show", false);
        titleSelector = new LabelTextFieldOption("Slide Show Title");
        addButton = new Button("Add Slide");
        this.model = model;
        slides = new SlidesEditor(model);
        
        getChildren().addAll(getComponentTitleLabel(),titleSelector,addButton,slides);
        
        initEventHandlers();
    }
    
    public SlidesEditor getSlidesEditor(){ return slides; }
    public LabelTextFieldOption getTitleSelector(){ return titleSelector; }
    
    public void initEventHandlers(){
        titleSelector.getText().textProperty().addListener(e->{
            model.setTitle(titleSelector.getText().getText());
        });
        
        addButton.setOnAction(e->{
           model.addSlide(PATH_IMAGES + "NoImage.png", "");
        });
        
        this.setOnMouseClicked(e->{
            controller.handleComponentClicked(this, model);
        });
    }
}
