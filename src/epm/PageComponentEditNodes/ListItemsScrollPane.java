/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epm.PageComponentEditNodes;

import epm.InputOptions.LabelTextFieldOption;
import epm.PageComponentModel.ListComponentModel;
import eportfoliomaker.ScreenSizeSingleton;
import java.util.ArrayList;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.VBox;

/**
 *
 * @author briankondracki
 */
public class ListItemsScrollPane extends ScrollPane{
    private VBox listItems;
    private ArrayList<ListItem>items;
    ScreenSizeSingleton screenSize;
    private ListComponentModel model;
    
    public ListItemsScrollPane(ListComponentModel model){
        listItems = new VBox();
        setContent(listItems);
        items = new ArrayList();
        
        this.model = model;
        
        screenSize = ScreenSizeSingleton.getInstance();
        listItems.setPrefHeight(screenSize.getHeight()*.2);
        setHbarPolicy(ScrollBarPolicy.NEVER);
        getStyleClass().add("component_scroll_pane");
    }
    
    public void addListItem(){
        ListItem newItem = new ListItem("-", items.size(), model);
        newItem.getText().setPrefWidth(screenSize.getWidth()*.45);
        newItem.getStyleClass().add("list_item");
        
        items.add(newItem);
        listItems.getChildren().add(newItem);
    }
    
    public void addListItem(String initText){
        ListItem newItem = new ListItem("-", items.size(), model);
        newItem.getText().setPrefWidth(screenSize.getWidth()*.45);
        newItem.getStyleClass().add("list_item");
        newItem.getText().setText(initText);
        
        items.add(newItem);
        listItems.getChildren().add(newItem);
    }
    
    public ArrayList getItems(){ return items; }
    
    public void removeListItem(int index){
        items.remove(index);
        listItems.getChildren().remove(index);
    }
}
