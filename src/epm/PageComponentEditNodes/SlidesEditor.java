/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epm.PageComponentEditNodes;

import epm.PageComponentModel.SlideModel;
import epm.PageComponentModel.SlideShowComponentModel;
import eportfoliomaker.ScreenSizeSingleton;
import java.util.ArrayList;
import java.util.Collections;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.layout.HBox;

/**
 *
 * @author Brian Kondracki
 */
public class SlidesEditor extends ScrollPane{
    private HBox slidesHBox;
    private ArrayList<SlideEditor> slides;
    private ScreenSizeSingleton screenSize;
    private SlideShowComponentModel model;
    
    public SlidesEditor(SlideShowComponentModel model){
        slidesHBox = new HBox();
        setContent(slidesHBox);
        slides= new ArrayList();
        
        this.model = model;
        
        screenSize = ScreenSizeSingleton.getInstance();
        slidesHBox.setPrefWidth(screenSize.getWidth()*.5);
        setVbarPolicy(ScrollBarPolicy.NEVER);
        getStyleClass().add("component_scroll_pane");
    }
    
    public SlideEditor addSlideEditor(SlideModel slideModel){
        SlideEditor slide = new SlideEditor(model,slideModel);
        
        slides.add(slide);
        slidesHBox.getChildren().add(slide);
        
        return slide;
    }
    
    public void removeSlideEditor(int index){
        slides.remove(index);
        slidesHBox.getChildren().remove(index);
    }
    
    public void moveSlideLeft(int index){
        Collections.swap(slides, index, index-1);
        slidesHBox.getChildren().clear();
        slidesHBox.getChildren().addAll(slides);
    }
    
    public void moveSlideRight(int index){
        Collections.swap(slides, index, index+1);
        slidesHBox.getChildren().clear();
        slidesHBox.getChildren().addAll(slides);
    }
    
    public ArrayList<SlideEditor> getSlides(){ return slides; }
    public HBox getSlidesHBox(){ return slidesHBox; }
}
