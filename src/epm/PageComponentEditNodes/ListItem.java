/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epm.PageComponentEditNodes;

import epm.PageComponentModel.ListComponentModel;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;

/**
 * 
 * @author briankondracki
 */
public class ListItem extends HBox {
    /**
     * @param container THE HBOX THAT HOLDS ALL OTHER ELEMENTS OF THIS OBJECT
     * @param label THE LABEL THAT TELLS THE USER WHAT TO ENTER IN THE TEXTBOX
     * @param textField THE TEXTFIELD THAT THE USER ENTERS TEXT INTO
     */
    private Label label;
    private TextField textField;
    private Button remove;
    private ListComponentModel model;
    private int index;
    
    /**
     * DEFAULT CONSTRUCTOR THAT ONLY ACCEPTS THE LABEL TEXT AS A PARAMETER
     * @param labelText 
     */
    public ListItem(String labelText, int index, ListComponentModel model){
        // CREATE ALL ELEMENTS OF THE LABELDROPDOWNOPTION OBJECT
        label = new Label(labelText);
        textField = new TextField();
        remove = new Button("X");
        
        this.index = index;
        this.model = model;
        System.out.println(index);
        
        setSpacing(5);
        
        initEventHandlers();
        
        // ADD THE LABEL AND DROPDOWN MENU TO THE HBOX
        getChildren().addAll(label,textField,remove);
    }
    
    /**
     * OVERLOADED CONSTRUCTOR THAT DISPLAYS INITIAL TEXT IN THE TEXTFIELD
     * @param labelText 
     * @param initTextFieldText
     */
    public ListItem(String labelText, String initTextFieldText, int index, ListComponentModel model){
        // CREATE ALL ELEMENTS OF THE LABELDROPDOWNOPTION OBJECT
        label = new Label(labelText);
        textField = new TextField(initTextFieldText);
        remove = new Button("X");
        
        this.index = index;
        this.model = model;
        
        initEventHandlers();
        
        // ADD THE LABEL AND DROPDOWN MENU TO THE HBOX
        getChildren().addAll(label,textField,remove);
    }
    
    /**
     * 
     * @return THE LABEL THAT IS BEING DISPLAYED
     */
    public Label getLabel(){
        return label;
    }
    
    /**
     * 
     * @return THE TEXTFIELD TAHT THE USER ENTERS INTO 
     */
    public TextField getText(){
        return textField;
    }
    
    public int getIndex(){ return index; }
    
    public void decrementIndex(){
        index--;
    }
    
    private void initEventHandlers(){
        textField.textProperty().addListener(e->{
            model.getListItems().set(index, textField.getText());
        });
        
        remove.setOnAction(e->{
            int index = model.getEditNode().getListItems().getItems().indexOf(this);
            model.removeListItem(index);
        });
    }
}
