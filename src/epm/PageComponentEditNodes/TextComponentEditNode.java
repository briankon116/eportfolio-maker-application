/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epm.PageComponentEditNodes;

import epm.InputOptions.LabelDropdownOption;
import epm.InputOptions.LabelTextFieldOption;
import epm.PageComponentModel.HyperlinkComponentModel;
import epm.PageComponentModel.TextComponentModel;
import epm.controller.EventController;
import java.util.ArrayList;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;

/**
 *
 * @author Brian Kondracki
 */
public class TextComponentEditNode extends PageComponentEditNode{
    /**
     * @param headerSelector The LabelTextFieldOption that allows the user to enter the header that will appear on the webpage.
     * @param fontSelector The LabelDropdownOption that allows the user to select which font this textfield will use on the webpage.
     * @param addHyperlinkButton Button that is only active when some text is highlighted in text. When this button is
     * pressed, a hyperlink is added to the link that is provided in the textfield.
     * @param text The TextField that the user enters the text that will appear on the webpage.
     */
    private LabelTextFieldOption titleSelector;
    private LabelDropdownOption fontSelector;
    private LabelDropdownOption hyperlinkViewer;
    private Button addHyperlinkButton;
    private TextArea text;
    private ArrayList<String>fonts;
    private ArrayList<HyperlinkComponentModel>hyperlinks;
    private TextComponentModel model;
    EventController controller = EventController.getInstance();
    
    /**
     * Default constructor for the TextComponentEditNode. No fonts are added to the font selector dropdown menu yet. Fonts must
     * be added later on using the addFont() method.
     */
    public TextComponentEditNode(TextComponentModel model){
        super("TextField",false);
        titleSelector = new LabelTextFieldOption("Header:");
        fonts = new ArrayList();
        fonts.add("Bitter");
        fonts.add("Montserrat");
        fonts.add("Josefin Slab");
        fonts.add("Lato");
        fonts.add("Oswald");
        fontSelector = new LabelDropdownOption("Font:",fonts);
        fontSelector.getDropdown().getSelectionModel().select(0);
        hyperlinks = new ArrayList();
        hyperlinkViewer = new LabelDropdownOption("Hyperlinks");
        addHyperlinkButton = new Button("Add New Hyperlink");
        text = new TextArea();
        
        text.setPrefHeight(getPrefWidth()*.2);
        text.setWrapText(true);
        
        this.model = model;
        
        getChildren().addAll(getComponentTitleLabel(),titleSelector,fontSelector,hyperlinkViewer,addHyperlinkButton,text);
  
        initEventHandlers();
    }
    
    /**
     * Overloaded constructor for the TextComponentEditNode. Sets the fonts to those provided in the input.
     * @param initFonts The fonts that are to be displayed in the dropdown menu.
     */
    public TextComponentEditNode(ArrayList<String> initFonts, ArrayList<HyperlinkComponentModel> initHyperlinks){
        super("TextField", false);
        titleSelector = new LabelTextFieldOption("Header:");
        fonts = initFonts;
        fontSelector = new LabelDropdownOption("Font:", fonts);
        hyperlinks = initHyperlinks;
        hyperlinkViewer = new LabelDropdownOption("Hyperlinks:");
        addHyperlinkButton = new Button("Add New Hyperlink");
        text = new TextArea();
        
        text.setPrefHeight(getPrefWidth()*.2);
        text.setWrapText(true);
        
        for(HyperlinkComponentModel h:hyperlinks)
            hyperlinkViewer.getDropdown().getItems().add(h.getText());
        
        getChildren().addAll(getComponentTitleLabel(),titleSelector,fontSelector,hyperlinkViewer,addHyperlinkButton,text);
        
        initEventHandlers();
    }
    
    /**
     * @return The header selector LabelTextFieldOption object.
     */
    public LabelTextFieldOption getTitleSelector(){
        return titleSelector;
    }
    
    /**
     * @return The font selector LabelDropdownObject. 
     */
    public LabelDropdownOption getFontSelector(){
        return fontSelector;
    }
    
    public TextComponentModel getModel(){ return model; }
    
    /**
     * @return The textfield that the user enters in the text for the paragraph.
     */
    public TextArea getText(){
        return text;
    }
    
    /**
     * @return The fonts that the user can select from in the dropdown menu.
     */
    public ArrayList<String> getFonts(){
        return fonts;
    }
    
    public ArrayList<HyperlinkComponentModel>getHyperlinks(){ return hyperlinks; }
    
    public LabelDropdownOption getHyperlinkViewer(){ return hyperlinkViewer; }
    
    /**
     * @return The add hyperlink TextFieldButtonOption object.
     */
    public Button getAddHyperLinkButton(){
        return addHyperlinkButton;
    }
    
    /**
     * Adds a new font to both the fonts arraylist as well as the font selector dropdown menu.
     * @param font 
     */
    public void addFont(String font){
        fonts.add(font);
        fontSelector.getDropdown().getItems().add(font);
    }
    
    public void addHyperlink(HyperlinkComponentModel hyperlink){
        hyperlinks.add(hyperlink);
        hyperlinkViewer.getDropdown().getItems().add(hyperlink.getText());
    }
    
    public void initEventHandlers(){
        titleSelector.getText().textProperty().addListener(e->{
            model.setTitle(titleSelector.getText().getText());
        });
            
        fontSelector.getDropdown().setOnAction(e->{
            model.setFont(fontSelector.getDropdown().getSelectionModel().getSelectedIndex()+1);
        });
        
        hyperlinkViewer.getDropdown().setOnAction(e->{
            if(hyperlinkViewer.getDropdown().getSelectionModel().getSelectedIndex()>-1){
                model.getHyperlinks().get(hyperlinkViewer.getDropdown().getSelectionModel().getSelectedIndex()).getHyperlinkEditNode().showEditNode();
            }
        });
        
        addHyperlinkButton.setOnAction(e->{
            if(text.getSelection().getStart()!= text.getSelection().getEnd()){
                String selectedText = text.getText(text.getSelection().getStart(), text.getSelection().getEnd());
                model.addHyperlink("test", selectedText, text.getSelection().getStart(), text.getSelection().getEnd());
                addHyperlink(model.getHyperlinks().get(model.getHyperlinks().size()-1));
                model.getHyperlinks().get(model.getHyperlinks().size()-1).getHyperlinkEditNode().showEditNode();
            }
        });
        
        text.textProperty().addListener(e->{
            model.setText(text.getText());
        });
        
        this.setOnMouseClicked(e->{
            controller.handleComponentClicked(this, model);
        });
    }
}
