/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epm.InputOptions;

import static eportfoliomaker.StartupConstants.CSS_CLASS_LABEL;
import static eportfoliomaker.StartupConstants.CSS_CLASS_LABEL_DROPDOWN;
import java.util.ArrayList;
import javafx.scene.control.Label;
import javafx.scene.control.ComboBox;
import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.HBox;

/**
 *
 * @author Brian Kondracki
 */
public class LabelMenuButtonOption extends HBox {
    /**
     * @param label THE LABEL THAT WILL INSTRUCT THE USER WHAT THE COMBOBOX IS FOR
     * @param dropdown THE DROPDOWN MENU THAT WILL CONTAIN ALL OF POSSIBLE OPTIONS FOR THE USER
     */
    private Label label;
    private MenuButton dropdown;
    
    /**
     * THE DEFAULT CONSTRUCTOR FOR THE LABELDROPDOWNOPTION OBJECT 
     */
    public LabelMenuButtonOption(String labelText){
        label = new Label(labelText);
        dropdown = new MenuButton();
        
        getChildren().addAll(label,dropdown);
        
        // CSS STYLE
        label.getStyleClass().add(CSS_CLASS_LABEL);
        getStyleClass().add(CSS_CLASS_LABEL_DROPDOWN);
    }
    
    public LabelMenuButtonOption(String labelText, ArrayList<String>dropdownOptions){
        // CREATE ALL ELEMENTS OF THE LABELDROPDOWNOPTION OBJECT
        label = new Label(labelText);
        dropdown = new MenuButton();
        
        // ADD ALL STRINGS FROM DROPDOWNOPTIONS ARRAY TO THE DROPDOWN MENU
        for(String s: dropdownOptions)
            dropdown.getItems().add(new MenuItem(s));
        
        // ADD THE LABEL AND DROPDOWN MENU TO THE HBOX
        getChildren().addAll(label,dropdown);
    }
    
    /**
     * @return THE LABEL THAT INSTRUCTS THE USER WHAT THE DROPDOWN MENU IS FOR
     */
    public Label getLabel(){
        return label;
    }
    
    /**
     * @return THE DROPDOWN MENU THAT CONTAINS ALL OF THE USER'S OPTIONS
     */
    public MenuButton getDropdown(){
        return dropdown;
    }
}
