/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epm.InputOptions;

import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;

/**
 * 
 * @author briankondracki
 */
public class LabelTextFieldOption extends HBox {
    /**
     * @param container THE HBOX THAT HOLDS ALL OTHER ELEMENTS OF THIS OBJECT
     * @param label THE LABEL THAT TELLS THE USER WHAT TO ENTER IN THE TEXTBOX
     * @param textField THE TEXTFIELD THAT THE USER ENTERS TEXT INTO
     */
    private Label label;
    private TextField textField;
    
    /**
     * DEFAULT CONSTRUCTOR THAT ONLY ACCEPTS THE LABEL TEXT AS A PARAMETER
     * @param labelText 
     */
    public LabelTextFieldOption(String labelText){
        // CREATE ALL ELEMENTS OF THE LABELDROPDOWNOPTION OBJECT
        label = new Label(labelText);
        textField = new TextField();
        
        setSpacing(5);
        
        // ADD THE LABEL AND DROPDOWN MENU TO THE HBOX
        getChildren().addAll(label,textField);
    }
    
    /**
     * OVERLOADED CONSTRUCTOR THAT DISPLAYS INITIAL TEXT IN THE TEXTFIELD
     * @param labelText 
     * @param initTextFieldText
     */
    public LabelTextFieldOption(String labelText, String initTextFieldText){
        // CREATE ALL ELEMENTS OF THE LABELDROPDOWNOPTION OBJECT
        label = new Label(labelText);
        textField = new TextField(initTextFieldText);
        
        // ADD THE LABEL AND DROPDOWN MENU TO THE HBOX
        getChildren().addAll(label,textField);
    }
    
    /**
     * 
     * @return THE LABEL THAT IS BEING DISPLAYED
     */
    public Label getLabel(){
        return label;
    }
    
    /**
     * 
     * @return THE TEXTFIELD TAHT THE USER ENTERS INTO 
     */
    public TextField getText(){
        return textField;
    }
}
