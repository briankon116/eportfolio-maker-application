/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epm.InputOptions;

import static eportfoliomaker.StartupConstants.CSS_CLASS_DUAL_BUTTONS;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.layout.HBox;

/**
 *
 * @author Brian Kondracki
 */
public class DualButtons extends HBox{
    /**
     * @param container THE HBOX THAT ALL OTHER COMPONENTS WILL BE HELD IN
     * @param button1 THE BUTTON THAT WILL BE ON THE LEFT
     * @param button2 THE BUTTON THAT WILL BE ON THE RIGHT
     */
    
    private Button button1;
    private Button button2;
    
    public DualButtons(String button1Text,String button2Text){
        button1 = new Button(button1Text);
        button2 = new Button(button2Text);
        
        getChildren().addAll(button1,button2);
        
        // CSS CLASS
        getStyleClass().add(CSS_CLASS_DUAL_BUTTONS);
    }
    
    public Button getLeft(){ return button1; }
    public Button getRight(){ return button2; }
    public Button getButton(int index){ return (Button)getChildren().get(index); }
    
    public void addButton(String buttonText, boolean isActive){
        Button newButton = new Button(buttonText);
        newButton.setDisable(!isActive);
        getChildren().add(newButton);
    }
    
    public void setDisableAll(){
        for(Node b:getChildren()){
            b.setDisable(true);
        }
    }
    
    public void setEnableAll(){
        for(Node b:getChildren()){
            b.setDisable(false);
        }
    }
}
