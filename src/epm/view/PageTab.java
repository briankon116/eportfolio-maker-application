/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epm.view;

import javafx.scene.control.Tab;

/**
 *
 * @author briankondracki
 */
public class PageTab extends Tab{
    private ComponentEditView componentEditView;
    
    public PageTab(String title){
        setText(title);
        
        componentEditView = new ComponentEditView();
        setContent(componentEditView);
        setClosable(false);
    }
    
    public PageTab(String title, ComponentEditView componentEditView){
        setText(title);
        
        this.componentEditView = componentEditView;
        setContent(componentEditView);
        setClosable(false);
    }
    
    public ComponentEditView getComponentEditView(){ return componentEditView; }
}
