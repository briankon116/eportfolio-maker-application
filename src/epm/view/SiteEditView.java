/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epm.view;

import SiteModel.SiteModel;
import epm.controller.EventController;
import epm.toolbar.SiteToolBar;
import javafx.scene.layout.BorderPane;

/**
 *
 * @author Brian
 */
public class SiteEditView extends BorderPane{
     private SiteToolBar siteToolBar;
     private PageTabPane pageTabPane;
     private EventController controller;
     private SiteModel siteModel;
     
     public SiteEditView(SiteModel siteModel){
         controller = EventController.getInstance(this, siteModel);
         siteToolBar = new SiteToolBar();
         pageTabPane = new PageTabPane();
         setCenter(pageTabPane);
         setRight(siteToolBar);
         
         this.siteModel = siteModel;
     }
     
     public SiteToolBar getSiteToolBar(){ return siteToolBar; }
     public PageTabPane getPageTabPane(){ return pageTabPane; }
     
     
}
