/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epm.view;

import SiteModel.PageModel;
import epm.controller.EventController;
import static eportfoliomaker.StartupConstants.CSS_CLASS_TAB_PANE;
import javafx.geometry.Side;
import javafx.scene.control.TabPane;

/**
 *
 * @author briankondracki
 */
public class PageTabPane extends TabPane{
    private EventController controller;
    
    public PageTabPane(){
        setSide(Side.RIGHT);
        
        getStyleClass().add(CSS_CLASS_TAB_PANE);
        
        controller = EventController.getInstance();
        
        initEventHandlers();
    }
    
    public void addPage(PageModel newPage){
        getTabs().add(newPage.getPageTab());
    }
    
    public void removePage(int index){
        getTabs().remove(index);
    }
    
    public PageTab getCurrentTab(){ return (PageTab)getSelectionModel().getSelectedItem(); }
    public int getCurrentPageIndex(){ return getSelectionModel().getSelectedIndex(); }
    
    public void initEventHandlers(){
        getSelectionModel().selectedItemProperty().addListener(e->{
            controller.handleTabClicked();
        });
    }
}
