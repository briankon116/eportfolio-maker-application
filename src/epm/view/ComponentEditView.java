/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epm.view;

import epm.PageComponentEditNodes.BannerComponentEditNode;
import epm.PageComponentEditNodes.PageComponentEditNode;
import epm.PageComponentModel.BannerComponentModel;
import eportfoliomaker.ScreenSizeSingleton;
import java.util.ArrayList;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.Pane;

import javafx.scene.layout.VBox;



/**
 *
 * @author Brian Kondracki
 */
public class ComponentEditView extends Pane{
    private ArrayList<PageComponentEditNode>components;
    private VBox container;
    private VBox componentsVBox;
    private ScrollPane componentsScrollPane;
    private BannerComponentEditNode banner;
    
    public ComponentEditView(){
        components = new ArrayList();
        container = new VBox();
        componentsVBox = new VBox();
        componentsScrollPane = new ScrollPane(componentsVBox);
        
        // SET THE SIZE OF THE SCROLL PANE
        ScreenSizeSingleton screenSize = ScreenSizeSingleton.getInstance();
        componentsScrollPane.setPrefViewportHeight(screenSize.getHeight()*.5);
        componentsScrollPane.setPrefViewportWidth(screenSize.getWidth()*.5);
        
        //Create Banner Property Object
        banner = new BannerComponentEditNode(new BannerComponentModel("","", "",""));
        
        // FILL ELEMENTS INTO CONTAINER
        reloadComponents();
        
        // SET CSS CLASSES
        //container.getStyleClass().add("component_scroll_pane");
        
        // PUT THE CONTAINER INTO THE PANE
        getChildren().add(container);
    }
    
    public void addBanner(BannerComponentEditNode banner){
        this.banner = banner;
        reloadComponents();
    }
    
    public void addComponent(PageComponentEditNode p){
        components.add(p);
        reloadComponents();
    }
    
    public void removeComponent(PageComponentEditNode p){
        components.remove(p);
        reloadComponents();
    }
    
    public void reloadComponents(){
        // FIRST CLEAR THE CONTAINER OF ALL ELEMENTS IF THERE ARE ANY
        container.getChildren().clear();
        
        // THEN ADD IN THE BANNER COMPONENT
        container.getChildren().add(banner);
        
        // THEN POPULATE THE SCROLLPANE WITH ALL COMPONENTS AND INSERT IT INTO CONTAINER
        if(components.isEmpty())
            return;
        else{
            componentsVBox.getChildren().clear();
            for(PageComponentEditNode p: components){
                componentsVBox.getChildren().add(p);
            }
            container.getChildren().add(componentsScrollPane);
        }
    }
}
