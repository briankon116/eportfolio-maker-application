/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SiteModel;

import epm.view.SiteEditView;
import java.util.ArrayList;

/**
 *
 * @author briankondracki
 */
public class SiteModel {
    private String title;
    private ArrayList<PageModel> pages;
    private SiteEditView siteEditView;
    
    public SiteModel(){
        title = "My Site";
        pages = new ArrayList();
    }
    
    public String getSiteTitle(){ return title; }
    public ArrayList<PageModel> getPages(){ return pages; }
    
    public void addPageModel(PageModel newPage){
        pages.add(newPage);
    }
    
    public void removePage(int index, SiteEditView ui){
        pages.remove(index);
        ui.getPageTabPane().removePage(index);
    }
    
    public void addPage(PageModel newPage, SiteEditView ui){
        pages.add(newPage);
        ui.getPageTabPane().addPage(newPage);
    }
    
    public void loadPagesToScreen(SiteEditView ui){
        for(PageModel p: pages)
            ui.getPageTabPane().addPage(p);
    }
    
    public void setSiteTitle(String title){ this.title = title; }
    public void setSiteEditView(SiteEditView siteEditView){ this.siteEditView = siteEditView; }
}
