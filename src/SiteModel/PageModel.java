/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package SiteModel;

import epm.PageComponentEditNodes.BannerComponentEditNode;
import epm.PageComponentModel.BannerComponentModel;
import epm.PageComponentModel.PageComponentModel;
import epm.view.PageTab;
import static eportfoliomaker.StartupConstants.PATH_IMAGES;
import java.util.ArrayList;

/**
 *
 * @author briankondracki
 */
public class PageModel{
    private String title;
    private String fileName;
    private int template;
    private int colorScheme;
    private int font;
    private BannerComponentModel bannerModel;
    private ArrayList<PageComponentModel> components;
    private PageTab pageTab;
    private int selectedIndex;
    
    public PageModel(String title, String fileName){
        this.title = title;
        this.fileName = fileName;
        template = 1;
        colorScheme = 1;
        font = 1;
        components = new ArrayList();
        pageTab = new PageTab(title);
        bannerModel = new BannerComponentModel("","","", "");
        BannerComponentEditNode bannerEditNode = bannerModel.getEditNode();
        pageTab.getComponentEditView().addBanner(bannerEditNode);
        selectedIndex = -1;
    }
    
    public BannerComponentModel getBanner(){ return bannerModel; }
    public String getTitle(){ return title; }
    public String getFileName(){ return fileName; }
    public int getTemplate(){ return template; }
    public int getColorScheme(){ return colorScheme; }
    public int getFont(){ return font; }
    public ArrayList<PageComponentModel> getComponents(){ return components; } 
    public PageTab getPageTab(){ return pageTab; }
    public int getSelectedIndex(){ return selectedIndex; }
    
    public void addComponent(PageComponentModel component){
        components.add(component);
        pageTab.getComponentEditView().addComponent(component.getEditNode());
    }
    
    public void removeComponent(PageComponentModel component){
        components.remove(component);
        pageTab.getComponentEditView().removeComponent(component.getEditNode());
    }
    
    public void setTemplate(int index){ template = index; }
    public void setColorScheme(int index){ colorScheme = index; }
    public void setFont(int index) { font = index; }
    public void setSelectedIndex(int index){ this.selectedIndex = index; }
    
    public void setTitle(String title){
        this.title = title;
        setFileName(title);
    }
    
    public void setFileName(String title){
        this.fileName = title.replaceAll("\\s", "") + ".html";
    }
}
