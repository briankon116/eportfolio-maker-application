/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportfoliomaker;

/**
 *
 * @author Brian
 */
public class ScreenSizeSingleton {
    private double width;
    private double height;
    private static ScreenSizeSingleton singleton = null;
    
    private ScreenSizeSingleton(double width, double height){
        this.width = width;
        this.height = height;
    }
    
    public static ScreenSizeSingleton getInstance(double width, double height){
        if(singleton == null)
            singleton = new ScreenSizeSingleton(width, height);
        
        return singleton;
    }
    
    public static ScreenSizeSingleton getInstance(){
        return singleton;
    }
    
    public static double getWidth(){
        return singleton.width;
    }
    
    public static double getHeight(){
        return singleton.height;
    }
}
