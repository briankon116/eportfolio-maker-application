/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportfoliomaker;


import epm.view.EPortfolioMakerView;
import javafx.application.Application;
import javafx.scene.text.Font;
import javafx.stage.Stage;


/**
 *
 * @author briankondracki
 */
public class EPortfolioMaker extends Application{
    private EPortfolioMakerView ui = new EPortfolioMakerView();
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
    @Override
    public void start(Stage primaryStage){
        ui.startUI(primaryStage, "ePortfolio Maker");
    }
}
