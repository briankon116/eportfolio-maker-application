/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epm.view;

import epm.controller.EventController;
import epm.toolbar.SiteToolBar;
import javafx.scene.layout.BorderPane;

/**
 *
 * @author Brian
 */
public class SiteEditView extends BorderPane{
     private SiteToolBar siteToolBar;
     private PageTabPane pageTabPane;
     private EventController controller;
     
     public SiteEditView(){
         controller = EventController.getInstance(this);
         siteToolBar = new SiteToolBar();
         pageTabPane = new PageTabPane();
         pageTabPane.addPage("test", new ComponentEditView());
         setCenter(pageTabPane);
         setRight(siteToolBar);
     }
     
     public SiteToolBar getSiteToolBar(){ return siteToolBar; }
     public PageTabPane getPageTabPane(){ return pageTabPane; }
}
