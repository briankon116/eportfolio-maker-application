/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epm.view;

import eportfliomaker.ScreenSizeSingleton;
import static eportfliomaker.StartupConstants.PATH_SITES;
import java.io.File;
import java.net.MalformedURLException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.scene.layout.Pane;
import javafx.scene.web.WebView;

/**
 *
 * @author Brian
 */
public class SiteViewView extends Pane{
    WebView webview;
    
    public SiteViewView(String siteName){
        webview = new WebView();
        
        initUI(siteName);
        
        // SET THE SIZE
        webview.setPrefHeight(ScreenSizeSingleton.getHeight()*.9);
        webview.setPrefWidth(ScreenSizeSingleton.getWidth());
        
        getChildren().add(webview);
    }
    
    private void initUI(String siteName){
        File f = new File(PATH_SITES + siteName + "/index.html");
        try{
            webview.getEngine().load(f.toURI().toURL().toString());
        }catch(MalformedURLException e){
            Logger.getLogger(SiteViewView.class.getName()).log(Level.SEVERE, null,e);
        }
    }
}
