/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epm.controller;

import epm.PageComponentEditNodes.ImageComponentEditNode;
import epm.PageComponentEditNodes.ListComponentEditNode;
import epm.PageComponentEditNodes.PageComponent;
import epm.PageComponentEditNodes.SlideShowComponentEditNode;
import epm.PageComponentEditNodes.TextComponentEditNode;
import epm.PageComponentEditNodes.VideoComponentEditNode;
import epm.PageComponentModel.HyperlinkComponentModel;
import epm.toolbar.AddComponents;
import epm.toolbar.ComponentControls;
import epm.toolbar.PageControls;
import epm.toolbar.SiteToolBar;
import epm.view.ComponentEditView;
import epm.view.PageTab;
import epm.view.PageTabPane;
import epm.view.SiteEditView;

/**
 *
 * @author briankondracki
 */
public class EventController {
    private SiteEditView ui;
    private static EventController singleton = null;
    
    private EventController(SiteEditView ui){
        this.ui = ui;
    }
    
    public static EventController getInstance(SiteEditView ui){
        if(singleton == null)
            singleton = new EventController(ui);
        
        return singleton;
    }
    
    public static EventController getInstance(){
        return singleton;
    }
    
    public void handleAddTextComponent(){
        getComponentEditView().addComponent(new TextComponentEditNode());
    }
    
    public void handleAddListComponent(){
        getComponentEditView().addComponent(new ListComponentEditNode());
    }
    
    public void handleAddListItem(ListComponentEditNode list){
        list.getListItems().addListItem();
    }
    
    public void handleAddImageComponent(){
        getComponentEditView().addComponent(new ImageComponentEditNode());
    }
    
    public void handleAddVideoComponent(){
        getComponentEditView().addComponent(new VideoComponentEditNode());
    }
    
    public void handleAddSlideShowComponent(){
        getComponentEditView().addComponent(new SlideShowComponentEditNode());
    }
    
    public void handleAddSlideRequest(SlideShowComponentEditNode slides){
        slides.getSlidesEditor().addSlideEditor();
    }
    
    public void handleHyperlinkDropdownRequest(TextComponentEditNode t){
        int index=t.getHyperlinkViewer().getDropdown().getSelectionModel().getSelectedIndex();
        t.getHyperlinks().get(index).getEditNode().showEditNode();
    }
    
    public void handleAddHyperlinkRequest(TextComponentEditNode t){
        t.addHyperlink(new HyperlinkComponentModel(0,0,"tset","Test"));
        t.getHyperlinks().get(t.getHyperlinks().size()-1).getEditNode().showEditNode();
    }
    
    public void handleComponentSelectRequest(PageComponent p){
        getComponentEditView().setSelectedComponent(p);
        getComponentControlsPanel().getControls().setEnableAll();
    }
    
    public SiteToolBar getSiteToolBar(){ return ui.getSiteToolBar(); }
    public PageControls getPageControlsPanel(){ return getSiteToolBar().getPageControlsPanel(); }
    public ComponentControls getComponentControlsPanel(){ return getSiteToolBar().getComponentControlsPanel(); }
    public AddComponents getAddComponentsPanel(){ return getSiteToolBar().getAddComponentsPanel(); }
    
    public PageTabPane getPageTabPane(){ return ui.getPageTabPane(); } 
    public PageTab getCurrentPageTab(){ return getPageTabPane().getCurrentTab(); }
    public ComponentEditView getComponentEditView(){ return getCurrentPageTab().getComponentEditView(); }
}
