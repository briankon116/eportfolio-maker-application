/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epm.toolbar;

import epm.InputOptions.DualButtons;
import epm.InputOptions.LabelDropdownOption;
import static eportfliomaker.StartupConstants.CSS_CLASS_TOOLBAR_PANEL;
import static eportfliomaker.StartupConstants.CSS_CLASS_TOOLBAR_TITLE;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;

/**
 *
 * @author bkondracki
 */
public class PageControls extends VBox{
    private Label title;
    private LabelDropdownOption pageTemplate;
    private LabelDropdownOption pageFont;
    private DualButtons movePageUpDownAddRemove;
    
    public PageControls(){
        title = new Label("Page Controls:");
        
        pageTemplate = new LabelDropdownOption("Page Template");
        pageTemplate.getDropdown().getItems().addAll("Template 1", "Template 2", "Template 3", "Template 4", "Template 5");
        pageTemplate.getDropdown().setPromptText("Select a Template");
        
        pageFont = new LabelDropdownOption("Page Font");
        pageFont.getDropdown().getItems().addAll("Font 1", "Font 2", "Font 3", "Font 4", "Font 5");
        pageFont.getDropdown().setPromptText("Select a Font");
        
        movePageUpDownAddRemove = new DualButtons("Add","Remove");
        movePageUpDownAddRemove.addButton("Move Up",false);
        movePageUpDownAddRemove.addButton("Move Down", false);
        movePageUpDownAddRemove.setDisableAll();
        movePageUpDownAddRemove.getChildren().get(0).setDisable(false);
        
        getChildren().addAll(title,pageTemplate, pageFont, movePageUpDownAddRemove);
        
        initEventHandlers();
        
        // CSS CLASS
        getStyleClass().add(CSS_CLASS_TOOLBAR_PANEL);
        pageTemplate.getStyleClass().add("page_control_input");
        pageFont.getStyleClass().add("page_control_input");
        title.getStyleClass().add(CSS_CLASS_TOOLBAR_TITLE);
    }
    ///chage later
    public LabelDropdownOption getPageTemplateControls(){ return pageTemplate; }
    public LabelDropdownOption getPageFontControls(){ return pageFont; }
    public DualButtons getMoveComponentUpDownRemoveControls(){ return movePageUpDownAddRemove; }
    
    public void initEventHandlers(){
        pageTemplate.getDropdown().setOnAction(e->{
            //todo
        });
        
        pageFont.getDropdown().setOnAction(e->{
            //todo
        });
        
        movePageUpDownAddRemove.getLeft().setOnMouseClicked(e->{
            //todo
        });
        
        movePageUpDownAddRemove.getRight().setOnMouseClicked(e->{
            //todo
        });
        
        movePageUpDownAddRemove.getChildren().get(2).setOnMouseClicked(e->{
            //todo
        });
        
        movePageUpDownAddRemove.getChildren().get(3).setOnMouseClicked(e->{
           //todo 
        });
    }
}
