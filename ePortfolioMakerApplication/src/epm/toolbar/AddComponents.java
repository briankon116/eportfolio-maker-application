/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epm.toolbar;

import epm.controller.EventController;
import static eportfliomaker.StartupConstants.CSS_CLASS_TOOLBAR_BUTTONS;
import static eportfliomaker.StartupConstants.CSS_CLASS_TOOLBAR_PANEL;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;

/**
 *
 * @author Brian
 */
public class AddComponents extends VBox{
    private Button newText;
    private Button newList;
    private Button newImage;
    private Button newVideo;
    private Button newSlideShow;
    
    EventController controller;
    
    public AddComponents(){
        newText = new Button("New Text Component");
        newList = new Button("New List Component");
        newImage = new Button("New Image Component");
        newVideo = new Button("New Video Component");
        newSlideShow = new Button("New Slide Show Component");
        getChildren().addAll(newText, newList, newImage, newVideo, newSlideShow);
        
        // CSS CLASS
        getStyleClass().add(CSS_CLASS_TOOLBAR_PANEL);
        getStyleClass().add(CSS_CLASS_TOOLBAR_BUTTONS);
        
        controller = EventController.getInstance();
        initEventHandlers();
    }
    
    public Button getNewText(){ return newText; }
    public Button getNewList(){ return newList; }
    public Button getNewImage(){ return newImage; }
    public Button getNewVideo(){ return newVideo; }
    public Button getNewSlideShow(){ return newSlideShow; }
    
    public void initEventHandlers(){
        newText.setOnMouseClicked(e->{
            controller.handleAddTextComponent();
        });
        
        newList.setOnAction(e->{
            controller.handleAddListComponent();
        });
        
        newImage.setOnAction(e->{
            controller.handleAddImageComponent();
        });
        
        newVideo.setOnAction(e->{
            controller.handleAddVideoComponent();
        });
        
        newSlideShow.setOnAction(e->{
            controller.handleAddSlideShowComponent();
        });
    }
}
