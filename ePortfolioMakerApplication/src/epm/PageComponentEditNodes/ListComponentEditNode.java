/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epm.PageComponentEditNodes;

import epm.InputOptions.LabelDropdownOption;
import epm.InputOptions.LabelTextFieldOption;
import java.util.ArrayList;
import javafx.scene.control.Button;

/**
 *
 * @author briankondracki
 */
public class ListComponentEditNode extends PageComponent{
    private LabelTextFieldOption titleSelector;
    private LabelDropdownOption fontSelector;
    private Button addListItemButton;
    private ArrayList<String>fonts;
    private ListItemsScrollPane listItems;
    
    public ListComponentEditNode(){
        super("List",false);
        titleSelector = new LabelTextFieldOption("Header:");
        fontSelector = new LabelDropdownOption("Fonts");
        addListItemButton = new Button("Add List Item");
        fonts = new ArrayList();
        listItems = new ListItemsScrollPane();
        
        getChildren().addAll(getComponentTitleLabel(), titleSelector,fontSelector,addListItemButton, listItems);
        
        initEventHandlers();
    }
    
    public ListItemsScrollPane getListItems(){ return listItems; }
    
    private void initEventHandlers(){
        fontSelector.getDropdown().setOnAction(e->{
           //todo 
        });
        
        addListItemButton.setOnAction(e->{
           getController().handleAddListItem(this); 
        });
    }
}
