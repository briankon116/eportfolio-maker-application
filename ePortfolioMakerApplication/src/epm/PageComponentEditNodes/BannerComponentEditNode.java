/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epm.PageComponentEditNodes;

import epm.InputOptions.LabelTextFieldOption;
import epm.InputOptions.TextFieldButtonOption;
import static eportfliomaker.StartupConstants.PATH_IMAGES;
import java.io.File;
import java.net.URL;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 *
 * @author briankondracki
 */
public class BannerComponentEditNode extends PageComponent{
    private LabelTextFieldOption bannerText;
    private LabelTextFieldOption footerText;
    private TextFieldButtonOption setImageButton;
    private Image thumbnail;
    private ImageView thumbnailView;
    
    public BannerComponentEditNode(){
        super("Banner", false);
        bannerText = new LabelTextFieldOption("Banner Text");
        footerText = new LabelTextFieldOption("Footer Text");
        setImageButton = new TextFieldButtonOption("Enter Banner Image URL", "Search");
        try{
            File file = new File(PATH_IMAGES + "NoImage.png");
            URL fileURL = file.toURI().toURL();

            thumbnail = new Image(fileURL.toExternalForm());
            thumbnailView = new ImageView(thumbnail);
        }catch(Exception e){
            System.out.println("No Image Found");
        }
        
        getChildren().addAll(getComponentTitleLabel(),bannerText, footerText, setImageButton, thumbnailView);
        
        initEventHandlers();
    }
    
    public LabelTextFieldOption getBannerText(){ return bannerText; }
    public LabelTextFieldOption getFooterText(){ return footerText; }
    public TextFieldButtonOption getSetImageButton(){ return setImageButton; }
    public Image getThumbnail(){ return thumbnail; }
    public ImageView getThumbnailView(){ return thumbnailView; }
    
    private void initEventHandlers(){
        bannerText.getText().setOnAction(e->{
            //todo
        });
        
        footerText.getText().setOnAction(e->{
           //todo 
        });
        
        setImageButton.getText().setOnAction(e->{
            //todo
        });
    }
}
