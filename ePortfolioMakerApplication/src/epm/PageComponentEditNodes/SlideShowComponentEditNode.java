/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epm.PageComponentEditNodes;

import epm.InputOptions.DualButtons;
import epm.InputOptions.LabelTextFieldOption;


/**
 *
 * @author briankondracki
 */
public class SlideShowComponentEditNode extends PageComponent{
    private LabelTextFieldOption titleSelector;
    private DualButtons addRemoveButtons;
    private SlidesEditor slides;
    
    public SlideShowComponentEditNode(){
        super("Slide Show", false);
        titleSelector = new LabelTextFieldOption("Slide Show Title");
        addRemoveButtons = new DualButtons("Add Slide", "Remove Slide");
        slides = new SlidesEditor();
        
        getChildren().addAll(getComponentTitleLabel(),titleSelector,addRemoveButtons,slides);
        
        initEventHandlers();
    }
    
    public SlidesEditor getSlidesEditor(){ return slides; }
    
    public void initEventHandlers(){
        titleSelector.getText().setOnAction(e->{
            //todo
        });
        
        addRemoveButtons.getLeft().setOnAction(e->{
           getController().handleAddSlideRequest(this);
        });
        
        addRemoveButtons.getRight().setOnAction(e->{
           //handleRemoveSlideRequest(); 
        });
    }
}
