/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epm.PageComponentEditNodes;

import epm.InputOptions.DualButtons;
import epm.controller.ImageSelectionController;
import static eportfliomaker.StartupConstants.PATH_IMAGES;
import java.io.File;
import java.net.URL;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;

/**
 *
 * @author briankondracki
 */
public class SlideEditor extends VBox{
    private Image thumbnail;
    private ImageView thumbnailView;
    private ImageSelectionController imageController;
    private TextField caption;
    private DualButtons leftRight;
    
    public SlideEditor(){
        try{
            File file = new File(PATH_IMAGES + "NoImage.png");
            URL fileURL = file.toURI().toURL();

            thumbnail = new Image(fileURL.toExternalForm());
            thumbnailView = new ImageView(thumbnail);
        }catch(Exception e){
            System.out.println("Slide show error");
        }
        imageController = new ImageSelectionController();
        caption = new TextField();
        leftRight = new DualButtons("<-","->");
        
        getChildren().addAll(thumbnailView,caption,leftRight);
    }
    
    public void setImage(String path, String fileName){
        caption.setText(path + fileName);
        thumbnail = new Image(path + fileName);
        thumbnailView.setImage(thumbnail);
        
        // todo: set image of slide in slideshow model
    }
    
    public void initEventHandlers(){
        thumbnailView.setOnMouseClicked(e->{
            imageController.processSelectImage(this);
        });
        
        caption.setOnAction(e->{
            //todo
        });
        
        leftRight.getLeft().setOnAction(e->{
            //handleMoveSlideLeftRequest();
        });
        
        leftRight.getRight().setOnAction(e->{
           //handleMoveSlideRightRequest(); 
        });
        
        this.setOnKeyPressed(e->{
           //handleSelectSlideRequest();
        });
    }
}
