/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epm.PageComponentEditNodes;

import epm.InputOptions.LabelTextFieldOption;
import eportfliomaker.ScreenSizeSingleton;
import java.util.ArrayList;
import javafx.scene.control.ScrollPane;
import javafx.scene.layout.VBox;

/**
 *
 * @author briankondracki
 */
public class ListItemsScrollPane extends ScrollPane{
    private VBox listItems;
    private ArrayList<LabelTextFieldOption>items;
    ScreenSizeSingleton screenSize;
    
    public ListItemsScrollPane(){
        listItems = new VBox();
        setContent(listItems);
        items = new ArrayList();
        
        screenSize = ScreenSizeSingleton.getInstance();
        listItems.setPrefHeight(screenSize.getHeight()*.2);
        setHbarPolicy(ScrollBarPolicy.NEVER);
        getStyleClass().add("component_scroll_pane");
    }
    
    public void addListItem(){
        LabelTextFieldOption newItem = new LabelTextFieldOption("-");
        newItem.getText().setPrefWidth(screenSize.getWidth()*.45);
        newItem.getStyleClass().add("list_item");
        
        items.add(newItem);
        listItems.getChildren().add(newItem);
    }
}
