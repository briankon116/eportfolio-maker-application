/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epm.PageComponentEditNodes;

import eportfliomaker.ScreenSizeSingleton;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.ScrollPane.ScrollBarPolicy;
import javafx.scene.layout.HBox;

/**
 *
 * @author Brian Kondracki
 */
public class SlidesEditor extends ScrollPane{
    private HBox slidesHBox;
    private ScreenSizeSingleton screenSize;
    
    public SlidesEditor(){
        slidesHBox = new HBox();
        setContent(slidesHBox);
        
        screenSize = ScreenSizeSingleton.getInstance();
        slidesHBox.setPrefWidth(screenSize.getWidth()*.5);
        setVbarPolicy(ScrollBarPolicy.NEVER);
        getStyleClass().add("component_scroll_pane");
    }
    
    public void addSlideEditor(){
        SlideEditor slide = new SlideEditor();
        
        slidesHBox.getChildren().add(slide);
    }
}
