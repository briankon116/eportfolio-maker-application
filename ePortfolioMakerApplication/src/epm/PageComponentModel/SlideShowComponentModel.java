/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epm.PageComponentModel;

import epm.PageComponentEditNodes.SlidesEditor;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

/**
 *
 * @author briankondracki
 */
public class SlideShowComponentModel {
    private SlidesEditor ui;
    private String title;
    private ObservableList<SlideModel> slides;
    private SlideModel selectedSlide;
    
    public SlideShowComponentModel(SlidesEditor initUI) {
	ui = initUI;
	slides = FXCollections.observableArrayList();
	reset();	
    }
    
    // ACCESSOR METHODS
    public boolean isSlideSelected() {
	return selectedSlide != null;
    }
    
    public ObservableList<SlideModel> getSlides() {
	return slides;
    }
    
    public SlideModel getSelectedSlide() {
	return selectedSlide;
    }

    public String getTitle() { 
	return title; 
    }
    
    public SlidesEditor getSlideShowMakerView(){
        return ui;
    }
    
    // MUTATOR METHODS
    public void setSelectedSlide(SlideModel initSelectedSlide) {
        selectedSlide = initSelectedSlide;
        //ui.reloadSlideShowPane(this);
        //ui.removeSlideButton.setDisable(false);
        //ui.moveSlideUpButton.setDisable(false);
        //ui.moveSlideDownButton.setDisable(false);
    }
    
    public void setTitle(String initTitle) { 
	title = initTitle; 
    }
    
    /**
     * Adds a slide to the slide show with the parameter settings.
     * @param initImageFileName File name of the slide image to add.
     * @param initImagePath File path for the slide image to add.
     */
    public void addSlide(String initImageFileName,String initImagePath, String initCaption, SlideShowComponentModel thisShow) {
	SlideModel slideToAdd = new SlideModel(initImageFileName, initImagePath, initCaption, this);
	slides.add(slideToAdd);
	selectedSlide = slideToAdd;
	//ui.reloadSlideShowPane(this);
    }
    
    public void removeSlide(SlideModel slideToRemove){
        int index;
        if(slides.size()>1){
            if(slides.indexOf(slideToRemove)==0)
                index = slides.indexOf(slideToRemove)+1;
            else
                index = slides.indexOf(slideToRemove)-1;
            selectedSlide=slides.get(index);
        }
        else
            selectedSlide=null;
        slides.remove(slideToRemove);
        //ui.reloadSlideShowPane(this);
    }
    
    public void moveSlideUp(SlideModel slideToMoveUp){
        if(slides.indexOf(slideToMoveUp)==0)
            return;
        else{
            int s= slides.indexOf(slideToMoveUp);
            slides.set(s, slides.get(s-1));
            slides.set(s-1, slideToMoveUp);
        }
        //ui.reloadSlideShowPane(this);
    }
    
     public void moveSlideDown(SlideModel slideToMoveDown){
        if(slides.indexOf(slideToMoveDown)==slides.size()-1)
            return;
        else{
            int s= slides.indexOf(slideToMoveDown);
            slides.set(s, slides.get(s+1));
            slides.set(s+1, slideToMoveDown);
        }
        //ui.reloadSlideShowPane(this);
    }
    
    /**
     * Resets the slide show to have no slides and a default title.
     */
    public void reset() {
	slides.clear();
	title = "Enter Slide Show Title";
	selectedSlide = null;
    }
}
