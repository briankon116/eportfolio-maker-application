/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epm.PageComponentModel;

import epm.PageComponentEditNodes.HyperlinkComponentEditNode;

/**
 *
 * @author briankondracki
 */
public class HyperlinkComponentModel extends PageComponentModel{
    private int startIndex;
    private int endIndex;
    private String url;
    private String text;
    private HyperlinkComponentEditNode editNode;
    
    public HyperlinkComponentModel(int initStartIndex, int initEndIndex, String initURL, String initText){
        super("Hyperlink");
        
        startIndex = initStartIndex;
        endIndex = initEndIndex;
        url = initURL;
        text = initText;
        
        editNode = new HyperlinkComponentEditNode(url);
    }
    
    public int getStartIndex(){ return startIndex; }
    public int getEndIndex(){ return endIndex; }
    public String getURL(){ return url; }
    public String getText(){ return text; }
    public HyperlinkComponentEditNode getEditNode(){ return editNode; }
}
