/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epm.InputOptions;

import static eportfliomaker.StartupConstants.CSS_CLASS_LABEL;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;

/**
 *
 * @author Brian
 */
public class LabelDualTextFieldOption extends HBox{
    private Label label;
    private TextField left;
    private TextField right;
    
    public LabelDualTextFieldOption(String labelText, String leftText, String rightText){
        label = new Label(labelText);
        left = new TextField();
        left.setPromptText(leftText);
        right = new TextField();
        right.setPromptText(rightText);
        
        // SET SIZE
        left.setPrefWidth(50);
        right.setPrefWidth(50);
        
        getChildren().addAll(label,left,right);
        
        label.getStyleClass().add(CSS_CLASS_LABEL);
        
        initEventHandlers();
    }
    
    public TextField getLeft(){
        return left;
    }
    
    public TextField getRight(){
        return right;
    }
    
    private void initEventHandlers(){
        left.setOnAction(e->{
            //todo
        });
        
        right.setOnAction(e->{
           //todo 
        });
    }
}
