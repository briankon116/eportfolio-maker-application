/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epm.view;

import epm.toolbar.SiteToolBar;
import static eportfliomaker.StartupConstants.CSS_CLASS_SITE_TOOL_BAR;
import javafx.scene.layout.BorderPane;

/**
 *
 * @author Brian
 */
public class SiteEditView extends BorderPane{
     private SiteToolBar siteToolBar;
     private ComponentEditView componentEditView;
     
     public SiteEditView(){
         siteToolBar = new SiteToolBar();
         siteToolBar.getStyleClass().add(CSS_CLASS_SITE_TOOL_BAR);
         componentEditView = new ComponentEditView();
         setCenter(componentEditView);
         setRight(siteToolBar);
     }
}
