/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epm.toolbar;

import javafx.scene.layout.VBox;

/**
 *
 * @author Brian
 */
public class SiteToolBar extends VBox{
    private PageControls pageControlsPanel;
    private AddComponents addComponentsPanel;
    
    public SiteToolBar(){
        pageControlsPanel = new PageControls();
        addComponentsPanel = new AddComponents();
        
        getChildren().addAll(pageControlsPanel, addComponentsPanel);
    }
    
    public PageControls getPageControlsPanel(){ return pageControlsPanel; }
    public AddComponents getAddComponentsPanel(){ return addComponentsPanel; }
}
