/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eportfliomaker;

/**
 *
 * @author Brian
 */
public class StartupConstants {
    public static String PATH_CSS = "epm/style/";
    public static String STYLE_SHEET_UI = PATH_CSS + "EPortfolioMakerStyle.css";
    public static String APP_FONT = "https://fonts.googleapis.com/css?family=Source+Sans+Pro";
    
    public static String CSS_CLASS_SITE_TOOL_BAR = "site_tool_bar";
    public static String CSS_CLASS_LABEL_DROPDOWN = "label_dropdown";
    public static String CSS_CLASS_LABEL = "label";
    public static String CSS_CLASS_TOOLBAR_PANEL = "toolbar_panel";
    public static String CSS_CLASS_DUAL_BUTTONS = "dual_buttons";
    public static String CSS_CLASS_FILE_TOOLBAR = "file_toolbar";
    public static String CSS_CLASS_TOOLBAR_BUTTONS = "toolbar_buttons";
    public static String CSS_CLASS_TAB_PANE = "tab_pane";
}
