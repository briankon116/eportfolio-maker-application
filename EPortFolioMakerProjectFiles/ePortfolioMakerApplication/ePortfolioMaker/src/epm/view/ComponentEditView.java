/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epm.view;

import epm.PageComponentEditNodes.PageComponent;
import java.util.ArrayList;
import javafx.scene.layout.VBox;

/**
 *
 * @author Brian
 */
public class ComponentEditView extends VBox{
    private ArrayList<PageComponent>components;
    
    public ComponentEditView(){
        components = new ArrayList();
        reloadComponents();
    }
    
    public void addComponent(PageComponent p){
        components.add(p);
        reloadComponents();
    }
    
    public void reloadComponents(){
        if(components.isEmpty())
            return;
        for(PageComponent p: components){
            getChildren().add(p);
        }
    }
}
