/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epm.PageComponentModel;

/**
 *
 * @author briankondracki
 */
public class ImageComponentModel extends PageComponentModel{
    private String path;
    private String fileName;
    
    public ImageComponentModel(){
        super("Image");
    }
    
    public ImageComponentModel(String title, String path, String fileName){
        super("Image");
        setTitle(title);
        this.path = path;
        this.fileName = fileName;
    }
    
    public String getPath(){ return path; }
    public String getFileName(){ return fileName; }
    
    public void setPath(String path){ this.path = path; }
    public void setFileName(String fileName){ this.fileName = fileName; }
}
