/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epm.PageComponentEditNodes;

import epm.InputOptions.LabelTextFieldOption;
import epm.InputOptions.TextFieldButtonOption;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;

/**
 *
 * @author Brian Kondracki
 */
public class ImageComponentEditNode extends PageComponent{
    /**
     * @param imageTitleSelector The LabelTextFieldOption object that allows the user to enter the title of the image. This title
     * will be displayed above the image as a header on the webpage.
     * @param setImageButton The TextFieldButtonOption object that allows the user to specify the location of the image they would
     * like to set on the webpage. When the button is pressed an imageselection window is opened so the user can search through 
     * their hard drive to find the image.
     * @param thumbnail The image object of the image to be displayed on the webpage.
     * @param thumbnailView The ImageView object that lets the user view a preview of the image that they want to display on the 
     * webpage before it is generated.
     */
    private LabelTextFieldOption titleSelector;
    private TextFieldButtonOption setImageButton;
    private Image thumbnail;
    private ImageView thumbnailView;
    
    /**
     * The only constructor for the ImageComponentEditNode object. There are no input variables since each object of this type will
     * have the same starting values.
     */
    public ImageComponentEditNode(){
        super("Image", false);
        titleSelector = new LabelTextFieldOption("Image Title");
        setImageButton = new TextFieldButtonOption("Enter Image URL", "Search");
        try{
            thumbnail = new Image("./images/NoImage.png");
            thumbnailView = new ImageView(thumbnail);
        }catch(Exception e){
            System.out.println("No Image Found");
        }
        
        getChildren().addAll(getComponentTitleLabel(),titleSelector,setImageButton,thumbnailView);
        
        initEventHandlers();
    }
    
    /**
     * @return The ImageTitleSelector object. 
     */
    public LabelTextFieldOption getTitleSelector(){ return titleSelector; }
    
    /**
     * @return The setImageButton object. 
     */
    public TextFieldButtonOption getSetImageButton(){ return setImageButton; }
    
    /**
     * @return The Image object for the thumbnail.
     */
    public Image getThumbnail(){ return thumbnail; }
    
    /**
     * @return The ImageView object for the thumbnail. 
     */
    public ImageView getThumbnailView(){ return thumbnailView; }
    
    /**
     * Initializes the eventhandlers to be used in this object.
     */
    public void initEventHandlers(){
        titleSelector.getText().setOnAction(e->{
            //todo
        });
        
        setImageButton.getText().setOnAction(e->{
            //todo
        });
    }
}
