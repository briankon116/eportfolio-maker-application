/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epm.PageComponentEditNodes;

import epm.InputOptions.LabelDropdownOption;
import epm.InputOptions.LabelTextFieldOption;
import epm.InputOptions.TextFieldButtonOption;
import java.util.ArrayList;

/**
 *
 * @author briankondracki
 */
public class VideoComponentEditNode extends PageComponent{
    /**
     * @param videoTitleSelector This is the LabelTextFieldOption object that allows the user to enter the title of the video.
     * @param URLSelector This is the TextFieldButtonOption that allows the user to select which video they want to include in
     * their webpage. They can either type in the URL in the searchbox or press the search button and select which video they want
     * using a GUI.
     * @parm videoSizeSelector This is the LabelDropdownOption object that allows the user to select what size they want their
     * video to appear as on their webpage. 
     */
    private LabelTextFieldOption titleSelector;
    private TextFieldButtonOption URLSelector;
    private LabelDropdownOption videoSizeSelector;
    
    /**
     * This is the default constructor for the VideoComponentEditNode. This accepts no variables as initializes the dropdown menu
     * with no options.
     */
    public VideoComponentEditNode(){
        super("Video", false);
        titleSelector = new LabelTextFieldOption("Video Title");
        URLSelector = new TextFieldButtonOption("Enter Video URL", "Search");
        videoSizeSelector = new LabelDropdownOption("Video Size");
        
        getChildren().addAll(getComponentTitleLabel(),titleSelector,URLSelector,videoSizeSelector);
        
        initEventHandlers();
    }
    
    /**
     * This is the overloaded constructor for the VideoComponentEditNode. This accepts on variable, videoSizes.
     * @param videoSizes This is an array list containing all of the video sizes available to the user.
     */
    public VideoComponentEditNode(ArrayList<String>videoSizes){
        super("Video",false);
        titleSelector = new LabelTextFieldOption("Video Title");
        URLSelector = new TextFieldButtonOption("Enter Video URL", "Search");
        videoSizeSelector = new LabelDropdownOption("Video Size", videoSizes);
        
        getChildren().addAll(getComponentTitleLabel(),titleSelector,URLSelector,videoSizeSelector);
        
        initEventHandlers();
    }
    
    /**
     * @return The videoTitleSelector object. 
     */
    public LabelTextFieldOption getTitleSelector(){ return titleSelector; }
    
    /**
     * @return The URLSelector object. 
     */
    public TextFieldButtonOption getURLSelector(){ return URLSelector; }
    
    /**
     * @return The videoSizeSelector object. 
     */
    public LabelDropdownOption getVideoSizeSelector(){ return videoSizeSelector; }
    
    public void initEventHandlers(){
        titleSelector.getText().setOnAction(e->{
            //todo
        });
        
        URLSelector.getText().setOnAction(e->{
            //todo
        });
        
        URLSelector.getButton().setOnAction(e->{
            //todo
        });
        
        titleSelector.getText().setOnAction(e->{
            //todo
        });
        
    }
}
