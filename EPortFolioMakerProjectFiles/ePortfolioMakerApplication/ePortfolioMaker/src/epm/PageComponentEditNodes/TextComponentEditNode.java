/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epm.PageComponentEditNodes;

import epm.InputOptions.LabelDropdownOption;
import epm.InputOptions.LabelTextFieldOption;
import epm.InputOptions.TextFieldButtonOption;
import java.util.ArrayList;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

/**
 *
 * @author Brian Kondracki
 */
public class TextComponentEditNode extends PageComponent{
    /**
     * @param headerSelector The LabelTextFieldOption that allows the user to enter the header that will appear on the webpage.
     * @param fontSelector The LabelDropdownOption that allows the user to select which font this textfield will use on the webpage.
     * @param addHyperlinkButton Button that is only active when some text is highlighted in text. When this button is
     * pressed, a hyperlink is added to the link that is provided in the textfield.
     * @param text The TextField that the user enters the text that will appear on the webpage.
     */
    private LabelTextFieldOption titleSelector;
    private LabelDropdownOption fontSelector;
    private TextFieldButtonOption addHyperlinkButton;
    private TextField text;
    private ArrayList<String>fonts;
    
    /**
     * Default constructor for the TextComponentEditNode. No fonts are added to the font selector dropdown menu yet. Fonts must
     * be added later on using the addFont() method.
     */
    public TextComponentEditNode(){
        super("TextField",false);
        titleSelector = new LabelTextFieldOption("Header:");
        fonts = new ArrayList();
        fontSelector = new LabelDropdownOption("Font:",fonts);
        addHyperlinkButton = new TextFieldButtonOption("Enter URL here","Enter");
        text = new TextField();
        
        getChildren().addAll(getComponentTitleLabel(),titleSelector,fontSelector,addHyperlinkButton,text);
        
        initEventHandlers();
    }
    
    /**
     * Overloaded constructor for the TextComponentEditNode. Sets the fonts to those provided in the input.
     * @param initFonts The fonts that are to be displayed in the dropdown menu.
     */
    public TextComponentEditNode(ArrayList<String> initFonts){
        super("TextField", false);
        titleSelector = new LabelTextFieldOption("Header:");
        fonts = initFonts;
        fontSelector = new LabelDropdownOption("Font:", fonts);
        addHyperlinkButton = new TextFieldButtonOption("Enter URL here","Enter");
        text = new TextField();
        
        getChildren().addAll(getComponentTitleLabel(),titleSelector,fontSelector,addHyperlinkButton,text);
        
        initEventHandlers();
    }
    
    /**
     * @return The header selector LabelTextFieldOption object.
     */
    public LabelTextFieldOption getTitleSelector(){
        return titleSelector;
    }
    
    /**
     * @return The font selector LabelDropdownObject. 
     */
    public LabelDropdownOption getFontSelector(){
        return fontSelector;
    }
    
    /**
     * @return The textfield that the user enters in the text for the paragraph.
     */
    public TextField getText(){
        return text;
    }
    
    /**
     * @return The fonts that the user can select from in the dropdown menu.
     */
    public ArrayList<String> getFonts(){
        return fonts;
    }
    
    /**
     * @return The add hyperlink TextFieldButtonOption object.
     */
    public TextFieldButtonOption getAddHyperLinkButton(){
        return addHyperlinkButton;
    }
    
    /**
     * Adds a new font to both the fonts arraylist as well as the font selector dropdown menu.
     * @param font 
     */
    public void addFont(String font){
        fonts.add(font);
        fontSelector.getDropdown().getItems().add(font);
    }
    
    public void initEventHandlers(){
        titleSelector.getText().setOnAction(e->{
            //todo
        });
            
        fontSelector.getDropdown().setOnAction(e->{
            //todo
        });
        
        addHyperlinkButton.getButton().setOnAction(e->{
            //todo
        });
        
        text.setOnAction(e->{
            //todo
        });
    }
}
