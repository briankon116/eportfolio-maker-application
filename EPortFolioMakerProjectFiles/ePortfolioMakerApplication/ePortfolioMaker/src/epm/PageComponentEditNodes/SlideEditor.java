/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epm.PageComponentEditNodes;

import epm.InputOptions.DualButtons;
import epm.controller.ImageSelectionController;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;

/**
 *
 * @author briankondracki
 */
public class SlideEditor extends VBox{
    private Image thumbnail;
    private ImageView thumbnailView;
    private ImageSelectionController imageController;
    private TextField caption;
    private DualButtons leftRight;
    
    public SlideEditor(){
        thumbnail = new Image("images/NoImage.jpg");
        thumbnailView = new ImageView(thumbnail);
        imageController = new ImageSelectionController();
        caption = new TextField();
        leftRight = new DualButtons("","");
        
        getChildren().addAll(thumbnailView,caption,leftRight);
    }
    
    public void setImage(String path, String fileName){
        caption.setText(path + fileName);
        thumbnail = new Image(path + fileName);
        thumbnailView.setImage(thumbnail);
        
        // todo: set image of slide in slideshow model
    }
    
    public void initEventHandlers(){
        thumbnailView.setOnMouseClicked(e->{
            imageController.processSelectImage(this);
        });
        
        caption.setOnAction(e->{
            //todo
        });
        
        leftRight.getLeft().setOnAction(e->{
            //handleMoveSlideLeftRequest();
        });
        
        leftRight.getRight().setOnAction(e->{
           //handleMoveSlideRightRequest(); 
        });
        
        this.setOnKeyPressed(e->{
           //handleSelectSlideRequest();
        });
    }
}
