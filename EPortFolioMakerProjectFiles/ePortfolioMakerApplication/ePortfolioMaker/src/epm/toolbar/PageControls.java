/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epm.toolbar;

import epm.InputOptions.DualButtons;
import epm.InputOptions.LabelDropdownOption;
import static eportfliomaker.StartupConstants.CSS_CLASS_TOOLBAR_PANEL;
import javafx.scene.layout.VBox;

/**
 *
 * @author bkondracki
 */
public class PageControls extends VBox{
    private LabelDropdownOption pageTemplate;
    private LabelDropdownOption pageFont;
    private DualButtons moveComponentUpDownRemove;
    
    public PageControls(){
        pageTemplate = new LabelDropdownOption("Page Template");
        pageTemplate.getDropdown().getItems().addAll("Template 1", "Template 2", "Template 3", "Template 4", "Template 5");
        pageTemplate.getDropdown().setPromptText("Select a Template");
        
        pageFont = new LabelDropdownOption("Page Font");
        pageFont.getDropdown().getItems().addAll("Font 1", "Font 2", "Font 3", "Font 4", "Font 5");
        pageFont.getDropdown().setPromptText("Select a Font");
        
        moveComponentUpDownRemove = new DualButtons("Move Up","Move Down");
        moveComponentUpDownRemove.addButton("Remove",false);
        
        moveComponentUpDownRemove.getLeft().setDisable(true);
        moveComponentUpDownRemove.getRight().setDisable(true);
        
        getChildren().addAll(pageTemplate, pageFont, moveComponentUpDownRemove);
        
        // CSS CLASS
        getStyleClass().add(CSS_CLASS_TOOLBAR_PANEL);
    }
    ///chage later
    public LabelDropdownOption getPageTemplateControls(){ return pageTemplate; }
    public LabelDropdownOption getPageFontControls(){ return pageFont; }
    public DualButtons getMoveComponentUpDownRemoveControls(){ return moveComponentUpDownRemove; }
    
    public void initEventHandlers(){
        pageTemplate.getDropdown().setOnAction(e->{
            //todo
        });
        
        pageFont.getDropdown().setOnAction(e->{
            //todo
        });
        
        moveComponentUpDownRemove.getLeft().setOnMouseClicked(e->{
            //todo
        });
        
        moveComponentUpDownRemove.getRight().setOnMouseClicked(e->{
            //todo
        });
        
        moveComponentUpDownRemove.getChildren().get(2).setOnMouseClicked(e->{
            //todo
        });
    }
}
