/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epm.toolbar;

import static eportfliomaker.StartupConstants.CSS_CLASS_TOOLBAR_BUTTONS;
import static eportfliomaker.StartupConstants.CSS_CLASS_TOOLBAR_PANEL;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;

/**
 *
 * @author Brian
 */
public class AddComponents extends VBox{
    private Button newText;
    private Button newImage;
    private Button newVideo;
    private Button newSlideShow;
    private VBox buttons;
    
    public AddComponents(){
        newText = new Button("New Text Component");
        newImage = new Button("New Image Component");
        newVideo = new Button("New Video Component");
        newSlideShow = new Button("New Slide Show Component");
        buttons = new VBox();
        buttons.getChildren().addAll(newText, newImage, newVideo, newSlideShow);
        
        getChildren().add(buttons);
        
        // CSS CLASS
        getStyleClass().add(CSS_CLASS_TOOLBAR_PANEL);
        buttons.getStyleClass().add(CSS_CLASS_TOOLBAR_BUTTONS);
    }
    
    public Button getNewText(){ return newText; }
    public Button getNewImage(){ return newImage; }
    public Button getNewVideo(){ return newVideo; }
    public Button getNewSlideShow(){ return newSlideShow; }
    
    public void initEventHandlers(){
        newText.setOnAction(e->{
            //todo
        });
        
        newImage.setOnAction(e->{
            //todo
        });
        
        newVideo.setOnAction(e->{
            //todo
        });
        
        newSlideShow.setOnAction(e->{
            //todo
        });
    }
}
