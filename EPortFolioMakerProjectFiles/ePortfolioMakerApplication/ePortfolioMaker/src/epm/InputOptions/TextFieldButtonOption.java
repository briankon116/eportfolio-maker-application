/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epm.InputOptions;

import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.HBox;

/**
 *
 * @author briankondracki
 */
public class TextFieldButtonOption extends HBox {
    /**
     * @param text The textfield of this component.
     * @param button The button of this component.
     */
    private TextField text;
    private Button button;
    
    /**
     * Default constructor for the TextFieldButton object. Does not set any text on the button or textfield.
     */
    public TextFieldButtonOption(){
        text = new TextField();
        button = new Button();
        
        getChildren().addAll(text,button);
    }
    
    /**
     * Overloaded constructor for the TextFieldButton object. Initial text is set on button or textfield or both, depending on the input.
     * @param initText The text that will be displayed in the textfield before the user enters anything. 
     * @param initButtonText The text that will be displayed on the button.
     */
    public TextFieldButtonOption(String initText, String initButtonText){
        text = new TextField(initText);
        button = new Button(initButtonText);
        
        getChildren().addAll(text,button);
    }
    
    /**
     * @return The textfield of this object.
     */
    public TextField getText(){
        return text;
    }
    
    /**
     * @return The button of this object.
     */
    public Button getButton(){
        return button;
    }
    
    /**
     * Sets the text displayed in the textfield before the user enters anything.
     * @param initText The initial text in the textfield.
     */
    public void setTextFieldText(String initText){
        text.setText(initText);
    }
    
    /**
     * Sets the text displayed on the button.
     * @param initText The text to be displayed on the button.
     */
    public void setButtonText(String initText){
        button.setText(initText);
    }
}
