/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epm.InputOptions;

import static eportfliomaker.StartupConstants.CSS_CLASS_LABEL;
import static eportfliomaker.StartupConstants.CSS_CLASS_LABEL_DROPDOWN;
import java.util.ArrayList;
import javafx.scene.control.Label;
import javafx.scene.control.ComboBox;
import javafx.scene.layout.HBox;

/**
 *
 * @author Brian Kondracki
 */
public class LabelDropdownOption extends HBox {
    /**
     * @param label THE LABEL THAT WILL INSTRUCT THE USER WHAT THE COMBOBOX IS FOR
     * @param dropdown THE DROPDOWN MENU THAT WILL CONTAIN ALL OF POSSIBLE OPTIONS FOR THE USER
     */
    private Label label;
    private ComboBox dropdown;
    
    /**
     * THE DEFAULT CONSTRUCTOR FOR THE LABELDROPDOWNOPTION OBJECT 
     */
    public LabelDropdownOption(String labelText){
        label = new Label(labelText);
        dropdown = new ComboBox();
        
        getChildren().addAll(label,dropdown);
        
        // CSS STYLE
        label.getStyleClass().add(CSS_CLASS_LABEL);
        getStyleClass().add(CSS_CLASS_LABEL_DROPDOWN);
    }
    
    public LabelDropdownOption(String labelText, ArrayList<String>dropdownOptions){
        // CREATE ALL ELEMENTS OF THE LABELDROPDOWNOPTION OBJECT
        label = new Label(labelText);
        dropdown = new ComboBox();
        
        // ADD ALL STRINGS FROM DROPDOWNOPTIONS ARRAY TO THE DROPDOWN MENU
        dropdown.getItems().addAll(dropdownOptions);
        
        // ADD THE LABEL AND DROPDOWN MENU TO THE HBOX
        getChildren().addAll(label,dropdown);
    }
    
    /**
     * @return THE LABEL THAT INSTRUCTS THE USER WHAT THE DROPDOWN MENU IS FOR
     */
    public Label getLabel(){
        return label;
    }
    
    /**
     * @return THE DROPDOWN MENU THAT CONTAINS ALL OF THE USER'S OPTIONS
     */
    public ComboBox getDropdown(){
        return dropdown;
    }
}
