/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epm.PageComponentModel;


/**
 *
 * @author briankondracki
 */
public class PageComponentModel {
    private String type;
    private String title;

    
    public PageComponentModel(String type){
        this.type = type; 
        title = "";
    }
    
    public String getType(){ return type; }
    public String getTitle(){ return title; }
    
    public void setType(String type){ this.type = type; }
    public void setTitle(String title){ this.title = title; }
}
