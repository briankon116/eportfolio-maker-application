package epm.PageComponentModel;

/**
 * This class represents a single slide in a slide show.
 * 
 * @author McKilla Gorilla & Brian Kondracki
 */
public class SlideModel {
    String imageFileName;
    String imagePath;
    String caption;
    SlideShowComponentModel thisSlideShow;
    boolean isSelected;
     
    /**
     * Constructor, it initializes all slide data.
     * @param initImageFileName File name of the image.
     * 
     * @param initImagePath File path for the image.
     * 
     */
    public SlideModel(String initImageFileName, String initImagePath, String initCaption, SlideShowComponentModel thisSlideShow) {
	imageFileName = initImageFileName;
	imagePath = initImagePath;
        this.caption = initCaption;
        this.thisSlideShow= thisSlideShow;
    }
    
    // ACCESSOR METHODS
    public String getImageFileName() { return imageFileName; }
    public String getImagePath() { return imagePath; }
    public String getCaption(){ return caption; }
    public SlideShowComponentModel getThisSlideShow(){ return thisSlideShow; }
    public boolean getIsSelected(){return isSelected;}
    
    // MUTATOR METHODS
    public void setImageFileName(String initImageFileName) {
	imageFileName = initImageFileName;
    }
    
    public void setImagePath(String initImagePath) {
	imagePath = initImagePath;
    }
    
    public void setImage(String initPath, String initFileName) {
	imagePath = initPath;
	imageFileName = initFileName;
    }
    
    public void setCaption(String caption){
        this.caption = caption;
    }
    
    public void setThisSlideShow(SlideShowComponentModel newSlideShow){
        thisSlideShow = newSlideShow;
    }
}
