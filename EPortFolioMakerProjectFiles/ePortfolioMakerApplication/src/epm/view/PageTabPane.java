/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epm.view;

import static eportfliomaker.StartupConstants.CSS_CLASS_TAB_PANE;
import javafx.geometry.Side;
import javafx.scene.control.TabPane;

/**
 *
 * @author briankondracki
 */
public class PageTabPane extends TabPane{
    public PageTabPane(){
        setSide(Side.RIGHT);
        
        getStyleClass().add(CSS_CLASS_TAB_PANE);
    }
    
    public void addPage(String title, ComponentEditView componentEditView){
        PageTab tab = new PageTab(title, componentEditView);
        
        getTabs().add(tab);
    }
}
