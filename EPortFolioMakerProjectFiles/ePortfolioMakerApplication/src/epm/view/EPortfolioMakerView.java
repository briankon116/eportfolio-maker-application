/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epm.view;

import epm.toolbar.FileToolbar;
import eportfliomaker.ScreenSizeSingleton;
import static eportfliomaker.StartupConstants.APP_FONT;
import static eportfliomaker.StartupConstants.CSS_CLASS_TAB_PANE;
import static eportfliomaker.StartupConstants.STYLE_SHEET_UI;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.layout.VBox;
import javafx.stage.Screen;
import javafx.stage.Stage;

/**
 *
 * @author Brian
 */
public class EPortfolioMakerView {
    // STAGE AND SCENE FOR THE EPORTFOLIO MAKER APPLICATION
    private Stage primaryStage;
    private Scene primaryScene;
    
    // SITEEDITVIEW OBJECT THAT WILL BE PLACED IN THE SITE EDIT TAB
    private SiteEditView siteEditView;
    
    // THE TABPANE THAT WILL HOLD THE SITEEDIT AND SITEVIEW WINDOWS
    private TabPane tabPane;
    private Tab siteEditTab;
    private Tab siteViewTab;
    
    // THE TOOLBAR
    private FileToolbar fileToolbar;
    
    public EPortfolioMakerView(){
        
    }
    
    public Stage getWindow(){ return primaryStage; }
    
    public void startUI(Stage initPrimaryStage, String windowTitle){
        initFileToolbar();
        
        Screen screen = Screen.getPrimary();
        Rectangle2D bounds = screen.getVisualBounds();
        
        ScreenSizeSingleton screenSize = ScreenSizeSingleton.getInstance(bounds.getWidth(), bounds.getHeight());
        
        initWorkspace();
        
        primaryStage = initPrimaryStage;
        initWindow(windowTitle);
    }
    
    private void initWorkspace(){
        siteEditView = new SiteEditView();
        //siteEditView.getStyleClass().add(set the stlye class);
        
         tabPane = new TabPane();
         siteEditTab = new Tab();
         siteEditTab.setText("Site Editor");
         siteEditTab.setClosable(false);
         siteEditTab.setContent(siteEditView);
         siteViewTab = new Tab();
         siteViewTab.setText("Site Viewer");
         siteViewTab.setClosable(false);
         tabPane.getTabs().addAll(siteEditTab, siteViewTab);
         tabPane.getStyleClass().add(CSS_CLASS_TAB_PANE);
    }
    
    private void initFileToolbar(){
        fileToolbar = new FileToolbar();
    }
    
    private void initWindow(String windowTitle){
        // SET WINDOW TITLE
        primaryStage.setTitle(windowTitle);

        Screen screen = Screen.getPrimary();
        Rectangle2D bounds = screen.getVisualBounds();
        
        // SET THE SIZE OF THE WINDOW
        primaryStage.setX(bounds.getMinX());
        primaryStage.setY(bounds.getMinY());
        primaryStage.setWidth(bounds.getWidth());
        primaryStage.setHeight(bounds.getHeight());
        
        //Set Height of workspace items
        tabPane.setPrefHeight(bounds.getHeight());
        
        // SETUP THE UI
        VBox workspace = new VBox();
        workspace.getChildren().addAll(fileToolbar,tabPane);
        primaryScene = new Scene(workspace);
        primaryScene.getStylesheets().add(STYLE_SHEET_UI);
        primaryScene.getStylesheets().add(APP_FONT);
        
        // SET THE SCENE TO THE WINDOW
        primaryStage.setScene(primaryScene);
        primaryStage.show();
    }
}
