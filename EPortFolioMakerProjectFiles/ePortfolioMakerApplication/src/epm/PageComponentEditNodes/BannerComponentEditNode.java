/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epm.PageComponentEditNodes;

import epm.InputOptions.TextFieldButtonOption;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;

/**
 *
 * @author briankondracki
 */
public class BannerComponentEditNode extends PageComponent{
    private TextFieldButtonOption setImageButton;
    private Image thumbnail;
    private ImageView thumbnailView;
    
    public BannerComponentEditNode(){
        super("Banner", false);
        setImageButton = new TextFieldButtonOption("Enter Banner Image URL", "Search");
        try{
            //thumbnail = new Image("./images/NoImage.png");
            //thumbnailView = new ImageView(thumbnail);
        }catch(Exception e){
            System.out.println("No Image Found");
        }
        
        getChildren().addAll(getComponentTitleLabel(),setImageButton);
        
        initEventHandlers();
    }
    
    public void initEventHandlers(){
        setImageButton.getText().setOnAction(e->{
            //todo
        });
    }
}
