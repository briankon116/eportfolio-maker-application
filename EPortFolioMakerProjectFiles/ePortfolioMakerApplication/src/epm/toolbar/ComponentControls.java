/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epm.toolbar;

import epm.InputOptions.DualButtons;
import static eportfliomaker.StartupConstants.CSS_CLASS_TOOLBAR_PANEL;
import static eportfliomaker.StartupConstants.CSS_CLASS_TOOLBAR_TITLE;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;

/**
 *
 * @author briankondracki
 */
public class ComponentControls extends VBox{
    private Label title;
    private DualButtons controls;
    
    public ComponentControls(){
        title = new Label("Component Controls:");
        
        controls = new DualButtons("Move Up", "Move Down");
        controls.addButton("Remove",false);
        controls.setDisableAll();
        
        getChildren().addAll(title,controls);
        
        initEventHandlers();
        
        // CSS CLASS
        getStyleClass().add(CSS_CLASS_TOOLBAR_PANEL);
        title.getStyleClass().add(CSS_CLASS_TOOLBAR_TITLE);
    }
    
    public void initEventHandlers(){
        controls.getLeft().setOnAction(e->{
            //todo
        });
        
        controls.getRight().setOnAction(e->{
            //todo
        });
        /*
        controls.getChildren().get(2).setOnMouseClicked(e->{
            //todo
        });
        
        controls.getChildren().get(3).setOnMouseClicked(e->{
            //todo
        });
                */
    }
}
