/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package epm.toolbar;

import static eportfliomaker.StartupConstants.CSS_CLASS_FILE_TOOLBAR;
import javafx.scene.control.Button;
import javafx.scene.control.ToolBar;

/**
 *
 * @author Brian
 */
public class FileToolbar extends ToolBar{
    private Button newButton;
    private Button loadButton;
    private Button saveButton;
    private Button exportButton;
    private Button exitButton;
    
    public FileToolbar(){
        newButton = new Button("New");
        loadButton = new Button("Load");
        saveButton = new Button("Save");
        exportButton = new Button("Export");
        exitButton = new Button("Exit");
        
        getItems().addAll(newButton, loadButton, saveButton, exportButton, exitButton);
        
        // CSS CLASS
        getStyleClass().add(CSS_CLASS_FILE_TOOLBAR);
        
        initEventHandlers();
    }
    
    private void initEventHandlers(){
        newButton.setOnAction(e->{
            //todo
        });
        
        loadButton.setOnAction(e->{
            //todo
        });
        
        saveButton.setOnAction(e->{
            //todo
        });
        
        exportButton.setOnAction(e->{
           //todo 
        });
        
        exitButton.setOnAction(e->{
           //todo 
        });
    }
}
