var components= new Array();
var navigationBarLinks = new Array();
var title;
var pageTitle;
var template;
var colorScheme;
var pageFont;
var div;
var jsonData;
var slideShowImages =new Array();
var slideShowCaptions = new Array();
var slideShowIndex = 0;
var play_pause_state=0;
var timer;
var pageName;
var page;
var bannerImage;
var bannerText;
var slideShowLength;


function initPage(){
    $.getJSON("site.json", function(data) {
        jsonData= data;
        getData(data);
    });
}

function getData(data){
    var pagePathName= window.location.pathname;
    pageName=pagePathName.substring(pagePathName.lastIndexOf("/")+1);

    if(pageName=="")
        pageName = "index.html";
    
    if(pageName=="index.html"){
        page=data.pages[0];
    }
    else{
        for(i =0; i<data.pages.length;i++){
            if(data.pages[i].fileName==pageName){
                console.log("tst");
                page=data.pages[i];
            }
        }

    }
    if(page!=undefined){
        title=page.title;

        $.each(data.pages, function(name,value){
            
            var li = document.createElement('li');
            li.innerHTML= '<a href=""></a>'
            if(value==data.pages[0])
                li.firstChild.href="index.html";
            else
                li.firstChild.href=value.fileName;
            li.firstChild.innerHTML=value.title;
            navigationBarLinks.push(li);
        });

       $.each(page,function(name,value){
          if(name=="title")
              pageTitle=value;
           else if(name=="template")
               template="template_" + value;
           else if(name=="colorScheme")
               colorScheme="colorScheme_" +value;
           else if(name=="banner"){
               if(value.bannerImageFileName!="")
                    bannerImage = value.bannerImageFileName;
               bannerText = value.bannerText;
               //if(value!="")
                 //  bannerImage=value;
           }
           else if (name=="font"){
               pageFont="font"+value;
           }
           else if(name=="components"){
               $.each(value, function(name,value){
                  components.push(value);
               });
           }
       });
        populatePage(data);
    }
}

function populatePage(data){
    console.log(template + "  " + colorScheme);
    document.getElementById("template").href="css/templates/" + template + ".css";
    document.getElementById("colorScheme").href="css/colorSchemes/" + colorScheme + ".css";
    document.getElementById("body").className=pageFont;
    document.getElementById("bannerText").innerHTML=bannerText;

    if(bannerImage!=undefined){
        document.getElementById("banner").className="body";
        var urlString = 'url(media/img/' + bannerImage + ')';
        document.getElementById("banner").style.backgroundImage=urlString;
    }
        
    
    $.each(navigationBarLinks, function(name,value){
        document.getElementById("navigationBarList").appendChild(value); 
    });
    
    $.each(components, function(name,value){
          if(value.type=="Textfield"){
              div = document.createElement('div');
              div.className = 'mainContent body font' + value.font;
              div.innerHTML = '<div class=content><div class=node><content id=textField><h3></h3></content></div></div>';
              
              
              div.lastChild.lastChild.lastChild.firstChild.innerHTML=value.title;
              var innerH = buildInlineLinks(value);
              var text = document.createElement('p');
              text.innerHTML=innerH;
              div.lastChild.lastChild.lastChild.appendChild(text);
              document.getElementById('body').appendChild(div);
          }
           
        if(value.type=="Image"){
              div = document.createElement('div');
              div.className = 'mainContent body';
              div.innerHTML = '<div class=content><div class=node><content><h2></h2><img class="image" src=""></content></div></div>';
              
            if(value.title!="")  
                div.lastChild.lastChild.lastChild.firstChild.innerHTML=value.title;  
            div.lastChild.lastChild.lastChild.lastChild.src='media/img/'+value.fileName;
              
              document.getElementById('body').appendChild(div);
              
          }
        
        if(value.type=="Video"){
              div = document.createElement('div');
              div.className = 'mainContent body';
              div.innerHTML = '<div class=content><div class=node><content><h2></h2><video class="image" src="" type="video/mp4" controls></content></div></div>';
              
            div.lastChild.lastChild.lastChild.firstChild.innerHTML=value.title;  
            div.lastChild.lastChild.lastChild.lastChild.src='media/video/' + value.fileName;
              
              document.getElementById('body').appendChild(div);
              
          }
        
        if(value.type=="Slide Show"){
            var i;    
            for(i = 0; i<value.slides.length;i++){
                    slideShowImages[i]= value.slides[i].imageFileName;
                    slideShowCaptions[i]=value.slides[i].caption;
                    console.log(value.slides[i].caption);
                }
            
              div = document.createElement('div');
              div.className = 'mainContent body';
              div.innerHTML = '<div class="content"><div class="node"><content><h2></h2><img id="slideShowImage" class="image" src=""><div class="buttons"><button id="previous" onClick="changeSlide(-1)">Previous</button><button id="play_pause" onClick="playPauseShow()">Play/Pause</button><button id="next" onClick="changeSlide(1)">Next</button></div><div id="slideShowCaption" class= "caption"></div></content></div></div>';
              
            div.lastChild.lastChild.lastChild.firstChild.innerHTML=value.title; 
            //div.lastChild.lastChild.lastChild.innerHTML=value.text;
              
              document.getElementById('body').appendChild(div);
            document.getElementById('slideShowImage').src = 'media/img/slideShowImages/'+ slideShowImages[0];
            document.getElementById('slideShowCaption').innerHTML=slideShowCaptions[0];
        }
        
        if(value.type=="List"){
            var div = document.createElement('div');
            div.className= 'mainContent body';
            div.innerHTML= '<div class="content"><div class="node"><content><h4></h4><ul class="list"></ul></content></div></div>';
            
            $.each(value.listItems, function(name,value){
                var li = document.createElement('li');
                li.innerHTML= value;
                div.lastChild.lastChild.lastChild.lastChild.appendChild(li);
        });
            div.lastChild.lastChild.lastChild.firstChild.innerHTML=value.title;
            document.getElementById('body').appendChild(div);
        }
    });
    document.title=page.title;
}

function changeSlide(num){
    length=slideShowImages.length-1;
    console.log(length);
    
    slideShowIndex+=num;
    
    if(slideShowIndex>length)
        slideShowIndex=0;
    if(slideShowIndex < 0)
        slideShowIndex=length;
    
    document.getElementById('slideShowImage').src='media/img/slideShowImages/' + slideShowImages[slideShowIndex];
    document.getElementById('slideShowCaption').innerHTML=slideShowCaptions[slideShowIndex];
}

function playPauseShow(){
    if(play_pause_state==0){
        play_pause_state=1;
        timer=setInterval(playSlideShow,1000);
    }
    else if(play_pause_state==1){
        play_pause_state=0;
        window.clearInterval(timer);
    }
}

function playSlideShow(){
    length=slideShowImages.length-1;
    
    slideShowIndex+=1;
    
    if(slideShowIndex>length)
        slideShowIndex=0;
    if(slideShowIndex<0)
        slideShowIndex=length;
    
    document.getElementById('slideShowImage').src='media/img/slideShowImages/' + slideShowImages[slideShowIndex];
    document.getElementById('slideShowCaption').innerHTML=slideShowCaptions[slideShowIndex];
}

function buildInlineLinks(value){
    var links=value.hyperlinks;
    var linkCount= links.length;
    var x= 0;
    var currentLink=links[x];
    var text = value.text;
    
    var textField="";
    
    if(linkCount==0)
        return value.text;
    
    for(i=0; i<text.length; i++){
        if(i==currentLink.startIndex){
            textField+='<a href="' + currentLink.url + '">' + text.substring(currentLink.startIndex,currentLink.endIndex) + '</a>';
            if(x+1<linkCount)
                x++;
            if(currentLink.endIndex<text.length){
                i=currentLink.endIndex;
            }

            currentLink=links[x];
        }
        textField+=text[i];
    }
    return textField;
}


